module.exports = {
  apps : [
    {
      name: "edu-web-dev",
      script: "npm",
      args: "run dev"
    },
    {
      name: "edu-web-prod",
      script: "npm",
      args: "run start"
    },
    {
      name: "edu-web-staging",
      script: "npm",
      args: "run start"
    }
  ]
}
