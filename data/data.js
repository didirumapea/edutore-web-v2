// let base_path = 'https://edutore.com/news/'

export const dataAccord = [
  {
    id: 1,
    title: 'Latihan Soal CPNS ONLINE',
    mlm: [],
    url: 'javascript:;'
  },
  {
    id: 2,
    title: 'Latihan Soal Psikotes Online',
    mlm: [],
    url: 'javascript:;'
  },
  {
    id: 3,
    title: 'SBMPTN',
    mlm: [
      {
        id: 31,
        title: 'Latihan Soal SBMPTN Saintek dan Soshum Online',
        mlm: [],
      },
      {
        id: 32,
        title: 'Test Kemampuan Dasar Umum (TKDU)',
        mlm: [
          {
            id: 321,
            title: 'Soal SBMPTN Matematika Dasar',
          },
          {
            id: 322,
            title: 'Soal SBMPTN Bahasa Indonesia',
          },
          {
            id: 323,
            title: 'Soal SBMPTN Bahasa Inggris',
          },
        ],
      },
      {
        id: 33,
        title: 'Soal TPA SBMPTN',
        mlm: [],
      },
      {
        id: 34,
        title: 'Soal TPS SBMPTN',
        mlm: [],
      },
      {
        id: 35,
        title: 'Soal SBMPTN Saintek',
        mlm: [
          {
            id: 351,
            title: 'Soal SBMPTN Matematika IPA Saintek',
            mlm: [],
          },
          {
            id: 352,
            title: 'Soal SBMPTN Fisika',
            mlm: [],
          },
          {
            id: 353,
            title: 'Soal SBMPTN Kimia',
            mlm: [],
          },
          {
            id: 354,
            title: 'Soal SBMPTN Biologi',
            mlm: [],
          },
        ],
      },
      {
        id: 36,
        title: 'Soal SBMPTN Soshum',
        mlm: [
          {
            id: 361,
            title: 'Soal SBMPTN Matematika IPS Soshum',
            mlm: [],
          },
          {
            id: 362,
            title: 'Soal SBMPTN Ekonomi',
            mlm: [],
          },
          {
            id: 363,
            title: 'Soal SBMPTN Sejarah',
            mlm: [],
          },
          {
            id: 364,
            title: 'Soal SBMPTN Geografi',
            mlm: [],
          },
          {
            id: 365,
            title: 'Soal SBMPTN Sosiologi',
            mlm: [],
          },
        ],
      }
    ],
  },
  {
    id: 4,
    title: 'SMA',
    mlm: [
      {
        id: 41,
        title: 'Latihan Soal Ujian Nasional SMA',
        mlm: [
          {
            id: 411,
            title: 'Soal Ujian Nasional - Matematika SMA (IPA)',
            mlm: [],
          },
          {
            id: 412,
            title: 'Soal Ujian Nasional - Matematika SMA (IPS)',
            mlm: [],
          },
          {
            id: 413,
            title: 'Soal Ujian Nasional - Bahasa Inggris (IPA)',
            mlm: [],
          },
          {
            id: 414,
            title: 'Soal Ujian Nasional - Kimia (IPA)',
            mlm: [],
          },
          {
            id: 415,
            title: 'Soal Ujian Nasional - Biologi SMA (IPA)',
            mlm: [],
          },
          {
            id: 416,
            title: 'Soal Ujian Nasional - Fisika SMA (IPA)',
            mlm: [],
          },
          {
            id: 447,
            title: 'Soal Ujian Nasional - Ekonomi SMA (IPA)',
            mlm: [],
          },
        ],
      },
      {
        id: 42,
        title: 'Matematika',
        mlm: [
          {
            id: 421,
            title: 'Soal Matematika SMA Kelas 10',
            mlm: [],
          },
          {
            id: 422,
            title: 'Soal Matematika SMA Kelas 11',
            mlm: [],
          },
          {
            id: 423,
            title: 'Soal Matematika SMA Kelas 12',
            mlm: [],
          },
        ],
      },
      {
        id: 43,
        title: 'Bahasa Inggris',
        mlm: [
          {
            id: 431,
            title: 'Soal Bahasa Inggris SMA Kelas 10',
            mlm: [],
          },
          {
            id: 432,
            title: 'Soal Bahasa Inggris SMA Kelas 11',
            mlm: [],
          },
          {
            id: 433,
            title: 'Soal Bahasa Inggris SMA Kelas 12',
            mlm: [],
          },
        ],
      },{
        id: 44,
        title: 'Fisika',
        mlm: [
          {
            id: 441,
            title: 'Soal Fisika SMA Kelas 10',
            mlm: [],
          },
          {
            id: 442,
            title: 'Soal Fisika SMA Kelas 11',
            mlm: [],
          },
          {
            id: 443,
            title: 'Soal Fisika SMA Kelas 12',
            mlm: [],
          },
        ],
      },
      {
        id: 45,
        title: 'Biologi',
        mlm: [
          {
            id: 451,
            title: 'Soal Biologi SMA Kelas 10',
            mlm: [],
          },
          {
            id: 452,
            title: 'Soal Biologi SMA Kelas 11',
            mlm: [],
          },
          {
            id: 453,
            title: 'Soal Biologi SMA Kelas 12',
            mlm: [],
          },
        ],
      },
      {
        id: 46,
        title: 'Kimia',
        mlm: [
          {
            id: 461,
            title: 'Soal Kimia SMA Kelas 10',
            mlm: [],
          },
          {
            id: 462,
            title: 'Soal Kimia SMA Kelas 11',
            mlm: [],
          },
          {
            id: 463,
            title: 'Soal Kimia SMA Kelas 12',
            mlm: [],
          },
        ],
      },
      {
        id: 47,
        title: 'Ekonomi',
        mlm: [
          {
            id: 471,
            title: 'Soal Ekonomi SMA Kelas 10',
            mlm: [],
          },
          {
            id: 472,
            title: 'Soal Ekonomi SMA Kelas 11',
            mlm: [],
          },
          {
            id: 473,
            title: 'Soal Ekonomi SMA Kelas 12',
            mlm: [],
          },
        ],
      },

    ],
  },
  {
    id: 5,
    title: 'SMP',
    mlm: [
      {
        id: 51,
        title: 'Latihan Soal UNBK dan USBN SMP MTs',
        mlm: [],
      },
      {
        id: 52,
        title: 'Matematika',
        mlm: [
          {
            id: 521,
            title: 'Soal Matematika SMP Kelas 7',
            mlm: [],
          },
          {
            id: 522,
            title: 'Soal Matematika SMP Kelas 8',
            mlm: [],
          },
          {
            id: 523,
            title: 'Soal Matematika SMP Kelas 9',
            mlm: [],
          },
        ],
      },
      {
        id: 53,
        title: 'Bahasa Inggris',
        mlm: [
          {
            id: 531,
            title: 'Soal Bahasa Inggris SMP Kelas 7',
            mlm: [],
          },
          {
            id: 532,
            title: 'Soal Bahasa Inggris SMP Kelas 8',
            mlm: [],
          },
          {
            id: 533,
            title: 'Soal Bahasa Inggris SMP Kelas 9',
            mlm: [],
          },
        ],
      },
    ],
  },
  {
    id: 6,
    title: 'Latihan Soal Ujian Sekolah SD',
    mlm: [],
  },
]



export const emails = [
   {
      value: false,
      id: 1,
      starred: true,
      inbox: true,
      draft: false,
      sent: false,
      spam: false,
      trash: false,
      userName: "Mitchell Miles",
      userEmail: "Mitchell@example.com",
      time: 1,
      avatarSrc: "/static/avatars/user-1.jpg",
      subject: "Pellentesque vel est a orci tempus viverra.",
      body: "Lorem Ipsum is simply dummy subject of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy subject ever since the 1500s,when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      attachments: [
         {
            id: 1,
            src: '/static/img/post-3.png',
            name: 'attachment-1.jpg'
         },
         {
            id: 2,
            src: '/static/img/post-4.png',
            name: 'attachment-2.jpg'
         },
         {
            id: 3,
            src: '/static/img/post-1.png',
            name: 'attachment-3.jpg'
         }
      ]
   },
   {
      value: false,
      id: 2,
      starred: false,
      inbox: true,
      draft: false,
      sent: false,
      spam: false,
      trash: false,
      userName: "Jhon Smith",
      userEmail: "Jhon@example.com",
      time: 2,
      avatarSrc: "/static/avatars/user-2.jpg",
      subject: "Sed ullamcorper dolor ac vulputate laoreet.",
      body: "Lorem Ipsum is simply dummy subject of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy subject ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      attachments: [
         {
            id: 1,
            src: '/static/img/post-3.png',
            name: 'attachment-1.jpg'
         },
         {
            id: 2,
            src: '/static/img/post-4.png',
            name: 'attachment-2.jpg'
         },
         {
            id: 3,
            src: '/static/img/post-1.png',
            name: 'attachment-3.jpg'
         }
      ]
   },
   {
      value: false,
      id: 3,
      starred: true,
      inbox: true,
      draft: false,
      sent: false,
      spam: false,
      trash: false,
      userName: "Merry Brown",
      userEmail: "Merry@example.com",
      time: 3,
      avatarSrc: "/static/avatars/user-3.jpg",
      subject: "Mauris venenatis massa ac mi consequat aliquam.",
      body: "Lorem Ipsum is simply dummy subject of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy subject ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      attachments: null
   },
   {
      value: false,
      id: 4,
      starred: false,
      inbox: true,
      draft: false,
      sent: false,
      spam: false,
      trash: false,
      userName: "Cristel Uno",
      userEmail: "Cristel@example.com",
      time: 7,
      avatarSrc: "/static/avatars/user-4.jpg",
      subject: "Donec feugiat lectus sit amet bibendum suscipit.",
      body: "Lorem Ipsum is simply dummy subject of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy subject ever since the 1500s,when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      attachments: [
         {
            id: 1,
            src: '/static/img/post-3.png',
            name: 'attachment-1.jpg'
         },
         {
            id: 2,
            src: '/static/img/post-4.png',
            name: 'attachment-2.jpg'
         },
         {
            id: 3,
            src: '/static/img/post-1.png',
            name: 'attachment-3.jpg'
         }
      ]
   },
   {
      value: false,
      id: 5,
      starred: true,
      inbox: true,
      draft: false,
      sent: false,
      spam: false,
      trash: false,
      userName: "Jone Miles",
      userEmail: "Jone@example.com",
      time: 4,
      avatarSrc: "/static/avatars/user-5.jpg",
      subject: "Nullam et enim id nulla porttitor consequat.",
      body: "Lorem Ipsum is simply dummy subject of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy subject ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      attachments: [
         {
            id: 1,
            src: '/static/img/post-3.png',
            name: 'attachment-1.jpg'
         },
         {
            id: 2,
            src: '/static/img/post-4.png',
            name: 'attachment-2.jpg'
         },
         {
            id: 3,
            src: '/static/img/post-1.png',
            name: 'attachment-3.jpg'
         }
      ]
   },
   {
      value: false,
      id: 6,
      starred: false,
      inbox: true,
      draft: false,
      sent: false,
      spam: false,
      trash: false,
      userName: "Michall Miles",
      userEmail: "Michall@example.com",
      time: 21,
      avatarSrc: "/static/avatars/user-6.jpg",
      subject: "Proin vitae lorem iaculis, fringilla tortor ut, varius dolor.",
      body: "Lorem Ipsum is simply dummy subject of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy subject ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      attachments: [
         {
            id: 1,
            src: '/static/img/post-3.png',
            name: 'attachment-1.jpg'
         },
         {
            id: 2,
            src: '/static/img/post-4.png',
            name: 'attachment-2.jpg'
         },
         {
            id: 3,
            src: '/static/img/post-1.png',
            name: 'attachment-3.jpg'
         }
      ]
   },
   {
      value: false,
      id: 7,
      starred: true,
      inbox: true,
      draft: false,
      sent: false,
      spam: false,
      trash: false,
      userName: "Rose Doe",
      userEmail: "Rose@example.com",
      time: 12,
      avatarSrc: "/static/avatars/user-7.jpg",
      subject: "Suspendisse id mauris sed orci iaculis vestibulum.",
      body: "Lorem Ipsum is simply dummy subject of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy subject ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      attachments: [
         {
            id: 1,
            src: '/static/img/post-3.png',
            name: 'attachment-1.jpg'
         },
         {
            id: 2,
            src: '/static/img/post-4.png',
            name: 'attachment-2.jpg'
         },
         {
            id: 3,
            src: '/static/img/post-1.png',
            name: 'attachment-3.jpg'
         }
      ]
   },
   {
      value: false,
      id: 8,
      starred: false,
      inbox: true,
      draft: false,
      sent: false,
      spam: false,
      trash: false,
      userName: "Krishna Miles",
      userEmail: "Krishna@example.com",
      time: 5,
      avatarSrc: "/static/avatars/user-8.jpg",
      subject: "Sed at purus lacinia, rhoncus lorem at, facilisis mauris.",
      body: "Lorem Ipsum is simply dummy subject of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy subject ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      attachments: [
         {
            id: 1,
            src: '/static/img/post-3.png',
            name: 'attachment-1.jpg'
         },
         {
            id: 2,
            src: '/static/img/post-4.png',
            name: 'attachment-2.jpg'
         },
         {
            id: 3,
            src: '/static/img/post-1.png',
            name: 'attachment-3.jpg'
         }
      ]
   },
   {
      value: false,
      id: 9,
      starred: false,
      inbox: true,
      draft: false,
      sent: false,
      spam: false,
      trash: false,
      userName: "Mitchell Rachel",
      userEmail: "Mitchell@example.com",
      time: 1,
      avatarSrc: "/static/avatars/user-9.jpg",
      subject: "Aliquam at dui eu dui placerat porta.",
      body: "Lorem Ipsum is simply dummy subject of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy subject ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      attachments: [
         {
            id: 1,
            src: '/static/img/post-3.png',
            name: 'attachment-1.jpg'
         },
         {
            id: 2,
            src: '/static/img/post-4.png',
            name: 'attachment-2.jpg'
         },
         {
            id: 3,
            src: '/static/img/post-1.png',
            name: 'attachment-3.jpg'
         }
      ]
   },
   {
      value: false,
      id: 10,
      starred: false,
      inbox: true,
      draft: false,
      sent: false,
      spam: false,
      trash: false,
      userName: "Jennifer Berganja",
      userEmail: "Jennifer@example.com",
      time: 2,
      avatarSrc: "/static/avatars/user-10.jpg",
      subject: "Welcome to our services",
      body: "Lorem Ipsum is simply dummy subject of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy subject ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      attachments: [
         {
            id: 1,
            src: '/static/img/post-3.png',
            name: 'attachment-1.jpg'
         },
         {
            id: 2,
            src: '/static/img/post-4.png',
            name: 'attachment-2.jpg'
         },
         {
            id: 3,
            src: '/static/img/post-1.png',
            name: 'attachment-3.jpg'
         }
      ]
   },
   {
      value: false,
      id: 11,
      starred: false,
      inbox: false,
      draft: false,
      sent: false,
      spam: false,
      trash: true,
      userName: "Jennifer Berganja",
      userEmail: "Jennifer@example.com",
      time: 2,
      avatarSrc: "/static/avatars/user-11.jpg",
      subject: "Welcome to our services",
      body: "Lorem Ipsum is simply dummy subject of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy subject ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      attachments: [
         {
            id: 1,
            src: '/static/img/post-3.png',
            name: 'attachment-1.jpg'
         },
         {
            id: 2,
            src: '/static/img/post-4.png',
            name: 'attachment-2.jpg'
         },
         {
            id: 3,
            src: '/static/img/post-1.png',
            name: 'attachment-3.jpg'
         }
      ]
   }
]

export const mailboxes = [
   {
      id: 1,
      name: 'Inbox',
      action: 'zmdi zmdi-inbox'
   },
   {
      id: 2,
      name: 'Draft',
      action: 'zmdi-email-open'
   },
   {
      id: 3,
      name: 'Starred',
      action: 'zmdi-star'
   },
   {
      id: 4,
      name: 'Sent',
      action: 'zmdi-mail-send'
   },
   {
      id: 5,
      name: 'Spam',
      action: 'zmdi-alert-circle'
   },
   {
      id: 6,
      name: 'Trash',
      action: 'zmdi-delete'
   }
]
