//import config from '~/store/config'
// STATE AS VARIABLE
export const state = () => ({
    myModule: null,
    isCheckedTime: false,
    searchWord: '',
    isLoading: false,
    list: ''
  })
// ACTIONS AS METHODS
  export const actions = { // asyncronous
    nuxtServerInit ({ commit }) {
      // console.log(new Date().getTime())
      //commit('setNuxtTime', (new Date()).getTime())
    },
    // SEARCH API AREA
    async getResultSearch ({ commit }, payload) {
        // let token = this.$cookies.get('user').token
        return await this.$axios.$get('api/v1/modules/search='+payload.word+'/page='+payload.pages+'/limit=12/sort_by=DESC')
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
            return response
        })
        .catch(err => {
            // alert(JSON.stringify(err.response))
            // console.log(err.response)
            console.log(err)

        })
      },
    async changePages ({ commit }, payload) {
        // let token = this.$cookies.get('user').token
        return await this.$axios.$get('api/v1/modules/search='+payload+'/page=1/limit=5/sort_by=DESC')
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
            return response
        })
        .catch(err => {
            // alert(JSON.stringify(err.response))
            console.log(err.response)
          //   return err.response
        })
      },
    // END SEARCH API AREA
    async getLikedUser ({ commit }) {
        let token = this.$cookies.get('user').token
        return await this.$axios.$get('api/v1/like-module', {headers : {'x-access-token': token}})
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
            return response
        })
        .catch(err => {
            // alert(JSON.stringify(err.response)
            // console.log(err.response)
            return err
        })
      },
    async liked ({ commit }, payload) {
        // console.log(payload)
        let token = this.$cookies.get('user').token
        return await this.$axios.$post('api/v1/like-module', {payload}, {headers : {'x-access-token': token}})
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
            return response
        })
        .catch(err => {
            // alert(JSON.stringify(err.response))
            // console.log(err.response)
            return err
        })
      },
    async getFilterModule ({ commit }, payload) {
        commit('showLoading')
        return await this.$axios.$get('api/v1/modules/filter/status='+payload.filter.status+'/kelas='+payload.filter.kelas+'/mata_pelajaran='+payload.filter.matPel+'/page='+payload.pages+'/limit=30')
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
        commit('hideLoading')
            return response
        })
        .catch(err => {
            // alert(JSON.stringify(err.response))
            console.log(err.response)
          //   return err.response
        })
      },
    async getModuleEdutest ({ commit }, payload) {
        // console.log(payload)
        // this.$axios.setToken(token, false)
        // console.log({Assesment})
        // {email: 'rohmat771@gmail.com', password: 'rahasia123'}
        return await this.$axios.$get('api/v1/modules/page='+payload.pages+'/limit=30/sort_by=ASC')
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
            return response
        })
        .catch(err => {
            // alert(JSON.stringify(err.response))
            console.log(err.response)
          //   return err.response
        })
      },
    async getModules ({ commit }, payload) {
        // console.log(payload)
        return await this.$axios.$get(`${process.env.API_URL}/modules/${payload}`)
        .then((response) => {
            // commit('set_user', response.data.user)
            // return { title: response.data }
          return response
        })
        .catch(err => {
            // error({ statusCode: 404, message: 'Maaf halaman yang anda cari tidak di temukan' })
            // alert(JSON.stringify(err.response))
            // console.log(err.response)
            return err.response.data
        })
      },
    async setMyModule ({ commit }) {
      return await this.$axios.$get('api/v1/my-modules', {headers : {'x-access-token': this.state.auth.user.token}})
        .then((response) => {
          // console.log(response.data.rows)
          commit('setMyModule', response.data.rows)
          return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          console.log(err.response)
          return err.response
          //   return err.response
        })
      },
    async getMyModule ({ commit }) {
          if (this.$cookies.get('my-module') !== undefined){
            commit('getMyModule', this.$cookies.get('my-module'))
          }
      },
    //    PAGES/EDUTEST
    async getKelas ({ commit }) {
            // console.log(payload)
            return await this.$axios.$get('api/v1/kelas')
            .then((response) => {
                // console.log(response)
                // commit('set_user', response.data.user)
                return response
            })
            .catch(err => {
                // alert(JSON.stringify(err.response))
                console.log(err.response)
            //   return err.response
            })
        },
    async getMatPel ({ commit }) {
            // console.log(payload)
            return await this.$axios.$get('api/v1/mata-pelajarans')
            .then((response) => {
                // console.log(response)
                // commit('set_user', response.data.user)
                return response
            })
            .catch(err => {
                // alert(JSON.stringify(err.response))
                // console.log(err.response)
            //   return err.response
            })
        },
        // MODULES/_ID
    async getMyScore ({ commit }, payload) {
            // console.log(payload)
            let token = this.$cookies.get('user').token
            return await this.$axios.$get('api/v1/assesment/modules/'+payload, {headers : {'x-access-token': token}})
            .then((response) => {
                // console.log(response)
                // commit('set_user', response.data.user)
                return response
            })
            .catch(err => {
                // alert(JSON.stringify(err.response))
                console.log(err.response)
            //   return err.response
            })
        },
  }
// MUTATION AS LOGIC
export const mutations = { // syncronous
    clearMyModules(state, payload){
      state.myModule = null
    },
    showLoading(state, payload){
      state.isLoading = true
    },
    hideLoading(state, payload){
      state.isLoading = false
    },
    setMyModule(state, payload){
        state.myModule = payload
    },
    getMyModule(state, payload){
        state.myModule = payload
    },
    setCheckedTime(state, payload){
        state.isCheckedTime = payload
        // console.log(payload)
    },
    setSearchWord(state, payload){
        state.searchWord = payload
        // console.log(payload)
    }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
    myModule: state => {
		return state.myModule
    },
    isCheckedTime: state => {
		return state.isCheckedTime
    },
    searchWord: state => {
		return state.searchWord
    },
}
