// import { resolve, reject } from "q";
// import firebase, {auth} from '~/services/fireinit'

import firebase, {auth} from '~/services/fireinit'


// STATE AS VARIABLE
export const state = () => ({
    savedQuestion: [],
    isLogin: false,
    isTokenExpired: false,
    user: {
        name: null,
        email: null,
        token: null,
        uid: null,
        photo: null,
    }
  })
// ACTIONS AS METHODS
export const actions = { // asyncronous
    async signInWithGoogle ({commit}){
      let provider = new firebase.auth.GoogleAuthProvider();
      // firebase.auth().getRedirectResult().then(function (result) {
      //   console.log(result)
      //   if (result.user === null) {
      //     // User not logged in, start login.
      //    firebase.auth().signInWithRedirect(provider);
      //   } else {
      //     // user logged in, go to home page.
      //     console.log('home');
      //   }
      // }).catch(function (error) {
      //   // Handle Errors here.
      //   console.log(error)
      //   // ...
      // });
      await auth.signInWithPopup(provider)
      .then((response) => {
          commit('set_social_login_user', response)
          console.log(response)
          // return response
      })
      // window.sessionStorage.setItem('pending', 1);
      // await auth.signInWithRedirect(provider)

      // console.log('hellow')
      // await auth.getRedirectResult().then(function(result) {
      //   console.log(result)
      //   if (result.credential) {
      //     console.log(result)
      //     // This gives you a Google Access Token. You can use it to access the Google API.
      //     var token = result.credential.accessToken;
      //     // ...
      //   }
      //   // The signed-in user info.
      //   var user = result.user;
      // }).catch(function(error) {
      //   // Handle Errors here.
      //   var errorCode = error.code;
      //   var errorMessage = error.message;
      //   // The email of the user's account used.
      //   var email = error.email;
      //   // The firebase.auth.AuthCredential type that was used.
      //   var credential = error.credential;
      //   // ...
      // });
    },
    async signInWithFacebook ({commit}){
      await auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then((response) => {
        commit('set_social_login_user', response)
          // console.log(response)
      })
    },
    async emailVerification ({ commit }, payload) {
      let user_id = payload
      return await this.$axios.$post('api/v1/verifications', {user_id})
      .then((response) => {
          // console.log(response)
          // commit('set_user', response)
          return response
      })
      .catch(err => {
        // alert(JSON.stringify(err.response))
        // console.log(err.response)
        return err.response
    })
    },
    async resetPassword ({ commit }, payload) {
      let email = payload
      return await this.$axios.$post('api/v1/reset-password', {email})
      .then((response) => {
          // console.log(response)
          // commit('set_user', response)
          return response
      })
      .catch(err => {
        // alert(JSON.stringify(err.response))
        // console.log(err.response)
        return err.response
    })
    },
    async registerUser ({ commit }, context) {
      // console.log(context)
      // {email: 'rohmat771@gmail.com', password: 'rahasia123'}
      return await this.$axios.$post('api/v1/register', context)
      .then((response) => {
          // console.log(response)
          // commit('set_user', response)
          return response
      })
      .catch(err => {
        //console.log(err)
        // alert(JSON.stringify(err.response))
        return err.response
    })
    },
    async resendEmailVerif ({ commit }, context) {
      // console.log(context)
      // {email: 'rohmat771@gmail.com', password: 'rahasia123'}
      return await this.$axios.$post('api/v1/resend-activations', {email: context})
        .then((response) => {
          // console.log(response)
          // commit('set_user', response)
          return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.log(err.response)
          return err.response
        })
    },
    async nuxtServerInit ({ commit }, context) {
        // console.log(context)
        // {email: 'rohmat771@gmail.com', password: 'rahasia123'}
        return await this.$axios.$post('api/v1/login', context)
        .then((response) => {
            // console.log(response)
            commit('set_user', response)
            // return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.log(err.response)
          return err.response
      })
      },
    async login ({ commit }, context) {
        // console.log(context)
        // {email: 'rohmat771@gmail.com', password: 'rahasia123'}
        return await this.$axios.$post('api/v1/login', context)
        .then((response) => {
            // console.log(response)
            commit('set_user', response)
            return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.log(err.response)
          return err.response
      })
      },
    setSavedQuestion(state, payload) {
        state.commit('setSavedQuestion', payload.payload)
        state.commit('setSavedDataSession', payload.payload2)
        // console.log(payload.payload)
        // console.log(payload.payload2)

    },
    getSavedQuestion(state, payload) {
        state.commit('getSavedQuestion', payload)
    },
    checkLogin(state) { // check login if page refresed || reload store
      // firebase.auth().onAuthStateChanged(function(user) {
      //   if (user) {
      //     console.log('this user sign in')
      //     // User is signed in.
      //   } else {
      //     console.log('no user sign in')
      //     // No user is signed in.
      //   }
      // });
      if (this.$cookies.get('user') === undefined) {
        state.isLogin = false
      }else{

        state.commit('get_user', this.$cookies.get('user'))

      }
      // console.log('state is login status : '+state.isLogin )
      // return state.isLogin
    },
    // logout(state) { // check login if page refresed || reload store
    //     // this.$cookies.get('user') === undefined ? state.isLogin = false : state.isLogin = true
    //   state.isLogin = false
    //   this.$cookies.removeAll();
    // },
    async logout ({commit}) {
      firebase.auth().signOut()
      .then(function() {
        // console.log('logout successful')
        // Sign-out successful.
      })
      .catch(function(error) {
        console.log('logout failed')
        // An error happened
      });

      commit('isLogout')
    },
    async reLogin ({commit}) {
      // TOken valid selama 1 jam.
      // console.log('hellow')
      let body = {
        grant_type: 'refresh_token',
        refresh_token: this.$cookies.get('user').refresh_token
      }
      return await this.$axios.$post('https://securetoken.googleapis.com/v1/token?key='+this.$cookies.get('user').api_key, body)
        .then((response) => {
          console.log(response)
          let data = {
              data: this.$cookies.get('user'),
              newToken: response.access_token
            }
                  // return data
          commit('setReloginUser', data)
          // commit('set_user', response)
          // return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.log(err)
          return err.response
        })


      // console.log(firebase.auth().currentUser.getIdToken())
      // console.log(this.$cookies.get('user'))
    //   firebase.auth().currentUser.getIdToken( true)
    // .then((idToken) => {
    //
    //   console.log(idToken)
    //       let data = {
    //         data: this.$cookies.get('user'),
    //         newToken: idToken
    //       }
    //       // return data
    //     // commit('setReloginUser', data)
    //   }).catch(function(error) {
    //     console.log('error')
    //   })
      // console.log(data)
    },
    async refreshToken ({commit}) {
    // TOken valid selama 1 jam.
    // console.log('hellow')
    let body = {
      grant_type: 'refresh_token',
      refresh_token: this.$cookies.get('user').refresh_token
    }
    return await this.$axios.$post(`get-token/token?key=${this.$cookies.get('user').api_key}`, body)
      .then((response) => {
        // console.log(response.access_token)
        let data = {
          data: this.$cookies.get('user'),
          newToken: response.access_token
        }
        // return data
        commit('refresh_token_user', data)
        // commit('set_user', response)
        // return response
      })
      .catch(err => {
        // alert(JSON.stringify(err.response))
        // console.log(err)
        return err.response
      })


    // console.log(firebase.auth().currentUser.getIdToken())
    // console.log(this.$cookies.get('user'))
    //   firebase.auth().currentUser.getIdToken( true)
    // .then((idToken) => {
    //
    //   console.log(idToken)
    //       let data = {
    //         data: this.$cookies.get('user'),
    //         newToken: idToken
    //       }
    //       // return data
    //     // commit('setReloginUser', data)
    //   }).catch(function(error) {
    //     console.log('error')
    //   })
    // console.log(data)
  },
    async testAuth({commit}){
    }
  }
// MUTATION AS LOGIC
export const mutations = { // syncronous
    refresh_token_user(state, payload){
      let dt = new Date(this.$moment().add(30, 'd').format())
      let dtToken = new Date(this.$moment().add(30, 'm').format())
      // console.log('OLD TOKEN : '+state.user.token)
      state.isTokenExpired = false
      state.user.name = payload.data.name
      state.user.email = payload.data.email
      state.user.token = payload.newToken
      state.user.photo = payload.data.photo
      state.user.uid = payload.data.uid
      state.isLogin = true
      // console.log('NEW TOKEN : '+ state.user.token)
      this.$cookies.set('user', state.user, {
        path: '/',
        expires: dt
      })
      this.$cookies.set('session-token', payload.newToken, {
        path: '/',
        expires: dtToken
      })
    },
    get_user(state, payload){
      state.user = payload
      state.isLogin = true
    },
    check_login(state) {
      if (this.$cookies.get('user') === undefined) {
        state.isLogin = false
      } else {
        state.user = this.$cookies.get('user')
        state.isLogin = true
      }
    },
    signOut(state, payload){
      state.user = {
        name: null,
        email: null,
        token: null,
        photo: null,
    }
    },
    set_social_login_user(state, data) {
        // console.log(data)
        state.user.name = data.user.displayName
        state.user.email = data.user.email
        state.user.token = data.user.ra
        state.user.photo = data.user.photoURL
        state.user.uid = data.user.uid
        state.isLogin = true
        // data.isSocialLogin = true
        // const cookieValObject = { param1: 'value1', param2: 'value2' }
        this.$cookies.set('user', state.user, {
          path: '/',
          maxAge: 60 * 60 * 24 * 7
        })
        // this.$cookies.set('user-details', data, {
        //   path: '/',
        //   maxAge: 60 * 60 * 24 * 7
        // })
        // console.log(state.user  )
    },
    setReloginUser(state, data) {
      // console.log(data.newToken)
      state.isTokenExpired = false
      state.user.name = data.data.name
      state.user.email = data.data.email
      state.user.token = data.newToken
      state.user.photo = data.data.photo
      state.user.uid = data.data.uid
      state.isLogin = true
      // console.log(state.user)
      this.$cookies.set('user', state.user, {
        path: '/',
        maxAge: 60 * 60 * 24 * 7
      })
    },
    SET_LOGIN(state, data) {
        state.isLogin = data
      },
    set_user (state, payload) {
      let dt = new Date(this.$moment().add(30, 'd').format())
      let dtToken = new Date(this.$moment().add(30, 'm').format())
      // console.log(payload.data.user.stsTokenManager.accessToken)
      if (payload.success){
         // state.user = data
      state.user.name = payload.data.user.displayName
      state.user.email = payload.data.user.email
      state.user.token = payload.data.user.stsTokenManager.accessToken
      state.user.photo = payload.data.user.photoURL
      state.user.uid = payload.data.user.uid
      state.user.api_key = payload.data.user.apiKey
      state.user.refresh_token = payload.data.user.stsTokenManager.refreshToken
      state.isLogin = true
      // const cookieValObject = { param1: 'value1', param2: 'value2' }
      this.$cookies.set('user', state.user, {
        path: '/',
        // maxAge: 60 * 60 * 24 * 7
        expires: dt
      })
      this.$cookies.set('session-token', payload.data.user.stsTokenManager.accessToken, {
        path: '/',
        expires: dtToken
      })
      // this.$cookies.set('user-details', data, {
      //   path: '/',
      //   maxAge: 60 * 60 * 24 * 7
      // })
      // console.log(state.user)
      }else{
        state.isLogin = false
      }

    },
    reset_user (state) {
      state.user = null
    },
    isLogout(state){
      state.isLogin = false
      state.user =  {
        name: null,
          email: null,
          token: null,
          uid: null,
          photo: null,
      }
      this.$cookies.removeAll();
    },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
    savedQuestion: state => {
		return state.savedQuestion
    },
    user: state => {
		return state.user
    },
    isLogin: state => {
      return state.isLogin
      },
    isTokenExpired: state => {
      return state.isTokenExpired
    }
}
