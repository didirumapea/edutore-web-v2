export default {
     state: () => ({
        list: [],
        image: 'this is image',
        product: []
      }),
      mutations: {
        add (state, text) {
            state.list.push({
            text: text,
            done: false
            })
        },
        remove (state, { todo }) {
            state.list.splice(state.list.indexOf(todo), 1)
        },
        toggle (state, todo) {
            todo.done = !todo.done
        }
    },
    getters: {
        image: state => {
            return state.image
        },
        product: state => {
            return state.product
        },
        list: state => {
            return state.list
        }
    }
}


  


