// STATE AS VARIABLE
export const state = () => ({
    mySubs: null,
    myListModuleSubs: null,
    myListModulesSubsId: [],
    myListModuleSubscription: [],
  })
// ACTIONS AS METHODS
  export const actions = { // asyncronous
    async getListLangganan ({ commit }) {
        return await this.$axios.get('api/v1/subscriptions')
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
            return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.log(err.response)
          return err.response
      })
      },
    async getDetailLangganan ({ commit }, payload) {
      return await this.$axios.get('api/v1/subscriptions/'+payload)
        .then((response) => {
          // console.log(response)
          // commit('set_user', response.data.user)
          return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.log(err.response)
          return err.response
        })
    },
    async setMySubscriptions ({ commit }) {
      if (this.$cookies.get('user').token !== undefined){
        let token = this.$cookies.get('user').token
        return await this.$axios.$get('api/v1/my-subscriptions', {headers : {'x-access-token': token}})
          .then(async (response) => {
            // console.log(response.data.rows)
            let listMyActiveSubs = []
            response.data.rows.forEach((element) => {
              let now = this.$moment().format('YYYY-MM-DD')
              let m = this.$moment(new Date(element.expire_at)).format('YYYY-MM-DD')
              let isExpired = this.$moment(now).isAfter(m, 'day');
              if (!isExpired){
                listMyActiveSubs.push(element)
              }
              // console.log(isExpired)
            })
            // console.log(listMyActiveSubs)
            commit('setMySubscriptions', listMyActiveSubs)
            return response
          })
          .catch(err => {
            // alert(JSON.stringify(err.response))
            console.log(err)
            return err.response
            //   return err.response
          })
      }
    },
    async getMyModuleSubscriptions ({ commit }, payload) {
      // console.log(payload)
      commit('loading/setTextLoading', 'my subscription', { root: true })
      if (this.$cookies.get('user').token !== undefined){
        let token = this.$cookies.get('user').token
        await this.$axios.$get('api/v1/my-subscriptions/'+payload, {headers : {'x-access-token': token}})
          .then((response) => {
            // console.log(response)
            // if (!response.status_expire){
              commit('setMyListSubscriptions', response)
            // }
            // return response
          })
          .catch(err => {
            // alert(JSON.stringify(err.response))
            console.log(err)
            return err.response
            //   return err.response
          })
      }
    },

  }
// MUTATION AS LOGIC
export const mutations = { // syncronous
  clearMySubscription(state){
    state.mySubs = null
  },
  clearMyListSubs(state){
    state.mySubs = []
    state.myListModuleSubs = []
    this.$cookies.remove('my-subscriptions')
    this.$cookies.remove('my-list-subscriptions')
  },
  getMySubscription(state, payload){
    // console.log(payload)
    if (this.$cookies.get('my-subscriptions') !== undefined){
      state.mySubs = this.$cookies.get('my-subscriptions')
    }
    // console.log(this.$cookies.get('my-subscriptions'))
  },
  getMyListSubscription(state){
    // console.log(payload)
    if (this.$cookies.get('my-list-subscriptions') !== undefined){
      state.myListModuleSubs = this.$cookies.get('my-list-subscriptions')
    }
    // console.log(this.$cookies.get('my-list-subscriptions'))
  },
  setMySubscriptions(state, payload){
    payload.forEach((element, index) => {
      payload[index].expire_at = this.$moment(element.expire_at).utc().format('DD MMM YYYY')
    })
    state.mySubs = payload
    // payload.forEach(element => {
      // let data = {
      //   id: element.id,
      //   userId: element.user_id
      // }
      // state.mySubs.push(data)
      // console.log(element)
    // })
    // console.log(payload)
    // this.$cookies.set('my-subscriptions', state.mySubs, {
    //     path: '/',
    //     maxAge: 60 * 60 * 24 * 7
    //   })
    // console.log(this.$cookies.get('my-subscriptions'))
  },
  setMyListSubscriptions(state, payload){
    if (state.myListModuleSubs === null){
      state.myListModuleSubs = []
    }
    if (!payload.status_expire){
      let subsId = payload.data.id
      let moduleSubs = payload.data.paket_langganan
      let data = {
        status_expire: payload.status_expire,
        idSubsModule: 0,
        listSubsModule: [],
      }
      data.idSubsModule = subsId
      moduleSubs.modules_langganan.forEach(element => {
        data.listSubsModule.push(element.modules.id)
      })
      state.myListModuleSubs.push(data)
      // state.myListModuleSubscription.push(data)
    }else{
      // if (state.myListModuleSubs === null){
      //   state.myListModuleSubs = []
      // }
    }
    // console.log(state.myListModuleSubscription)
    // console.log(state.myListModuleSubs)

    // this.$cookies.set('my-list-subscriptions', state.myListModuleSubs, {
    //   path: '/',
    //   maxAge: 60 * 60 * 24 * 7
    // })
    // console.log(state.myListModuleSubs)
    // state.myListModuleSubs += payload
    // this.$cookies.set('my-module', payload, {
    //     path: '/',
    //     maxAge: 60 * 60 * 24 * 7
    //   })
  },
  setMyListModuleSubscripion(state, payload){
    // console.log(payload)
    // state.myListModuleSubscription.push(payload)
  }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  mySubs: state => {
    return state.mySubs
  },
  myListModuleSubs: state => {
    return state.myListModuleSubs
  },
  myListModulesSubsId: state => {
    return state.myListModulesSubsId
  }
}
