// STATE AS VARIABLE
export const state = () => ({
    savedQuestion: [],
    isLogin: false,
    user: {
        name: null,
        email: null,
        token: null
    }
  })
// ACTIONS AS METHODS
  export const actions = { // asyncronous
    async registerUser ({ commit }, context) {
        // console.log(context)
        // {email: 'rohmat771@gmail.com', password: 'rahasia123'}
        return await this.$axios.$post('api/v1/register', context)
        .then((response) => {
            // console.log(response)
            commit('set_user', response.data.user)
            return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.log(err.response)
          return err.response
      })
      },
  }
// MUTATION AS LOGIC
export const mutations = { // syncronous
    SET_LOGIN(state, data) {
        state.isLogin = data
      },
      set_user (state, data) {
        // state.user = data
        state.user.name = data.displayName
        state.user.email = data.email
        state.user.token = data.stsTokenManager.accessToken
        state.isLogin = true
        // const cookieValObject = { param1: 'value1', param2: 'value2' }
        this.$cookies.set('user', state.user, {
          path: '/',
          maxAge: 60 * 60 * 24 * 7
        })
        this.$cookies.set('user-details', data, {
          path: '/',
          maxAge: 60 * 60 * 24 * 7
        })
        // console.log(state.user)
      },
      reset_user (state) {
        state.user = null
      }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
    savedQuestion: state => {
		return state.savedQuestion
    },
    user: state => {
		return state.user
    },
    isLogin: state => {
      return state.isLogin
      }
}
