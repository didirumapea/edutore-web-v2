import Vuex from 'vuex'
import home from './modules/home'

// JIKA INGIN LEBIH RAPIH GUNAKAN SISTEM INI

export const store = new Vuex.Store({
  // state: () => ({
  //   counter: 0
  // }),
  // mutations: {
  //   increment (state) {
  //     state.counter++
  //   }
  // },
  modules: {
    home
  }
})