// STATE AS VARIABLE
export const state = () => ({

  })
// ACTIONS AS METHODS
  export const actions = { // asyncronous
    async kritikSaran ({ commit }, context) {
        return await this.$axios.$post('api/v1/kritik-dan-saran', context)
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
            return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.l og(err.response)
          return err.response
        })
      },
  }
// MUTATION AS LOGIC
export const mutations = { // syncronous

}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  user: state => {
    return state.user
  },
}
