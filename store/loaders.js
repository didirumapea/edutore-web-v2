// import config from '~/store/config'

// STATE AS VARIABLE
export const state = () => ({
  email: 'example@ex.com',
  progress: 0
})
// ACTIONS AS METHODS
export const actions = { // asyncronous
  async nuxtServerInit({ commit }, { req }){
    // commit()
    // console.log('server init')
  },
  async sampleGet ({ commit }) {
    return await this.$axios.get(`users/get/order/page=1/limit=10/sort=desc`, {headers: this.state.auth.headers})
      .then((response) => {
        // console.log(response)
        commit('setListOrderUser', response.data)
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
        }
      })
  },
  async samplePost ({ commit }, payload) {
    return await this.$axios.post(`payment/order`, payload, {headers: this.state.auth.headers})
      .then((response) => {
        // console.log(response)
        // commit('setListOrderUser', response.data)
        return response.data
      })
      .catch(err => {
        if (err.response === undefined){
          return {
            success: false,
            message: err,
            info: 'no connection'
          }
          // console.log('No Connection')
        }else{
          console.log(err.response)
          return {
            success: false,
            message: err,
          }
        }
      })
  },
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
  addProgress(state, payload){
    // console.log(payload)
    state.progress = payload
    // this.$cookies.get('banner-top', payload)
  },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  sampleGetters: state => {
    return state.email
  },
}
