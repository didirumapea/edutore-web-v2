

// STATE AS VARIABLE
export const state = () => ({
    cacheJawabanSoalUser: [],
    examLoadingSoal: true,
    onGoingSoal: null,
    userScoreCache: null,
    userCacheSessionModule: null,
    cachePembahasan: null
  })
// ACTIONS AS METHODS
  export const actions = { // asyncronous
    nuxtServerInit ({ commit }, { req }) {
        if (req.session.user) {
          commit('user', req.session.user)
        }
      },
    // SOAL REGION
    // GET FROM INSIDE TEST
    async fillSoal ({commit}, payload) {
      // console.log(payload)
      let url = `soals/modules/${payload.id_module}/paket_soal/${payload.id_paket_soal}`
      // let headers =
      switch (payload.type_test){
        case 'trial':
          url = `trials/modules/${payload.id_module}/paket_soal/${payload.id_paket_soal}`
          break;
        case 'langganan':
          url = `my-subscribe-soal/${payload.id_langganan}/modules/${payload.id_module}/paket_soal/${payload.id_paket_soal}`
          break;
      }
      // console.log(tipe_test)
      return await this.$axios.$get(`api/v1/${url}`, {headers: {'x-access-token': this.state.auth.user.token}})
        .then((response) => {
          // console.log(response.data)
          commit('setFillSoal', response)
          commit('getCacheJawabanSoal')
          response.id_paket_soal = payload.id_paket_soal
          // state.commit('set_user', response.data.user)
          return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          console.log(err.response)
          return err.response
        })
      },
    // REGION SCORE
    async assesment({commit}, payload){
        let token = this.$cookies.get('user').token
        // console.log(payload)
        let data = {
            Assesment: [payload]
        }
        // console.log(data)
        this.$axios.$post('api/v1/assesment', data, {headers : {'x-access-token': token}})
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
        })
        .catch(err => {
            // alert(JSON.stringify(err.response))
            console.log(err.response)
        })
      },
    async laporkanSoal({commit}, payload){
        let token = this.$cookies.get('user').token
        let data = {
          paket_soal_id: payload.paketSoalId,
          soal_id: payload.soalId,
          alasan: payload.alasan
        }
        return await this.$axios.$post('api/v1/report-soal', data, {headers : {'x-access-token': token}})
          .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
            return response
          })
          .catch(err => {
            // alert(JSON.stringify(err.response))
            console.log(err.response)
          })
      }
  }
// MUTATION AS LOGIC
export const mutations = { // syncronous
    setFillSoal(state, payload){
      // STORE SOAL ON GOING DAN REMOVE LOADING AFTER LOAD SOAL FROM SERVER
      state.examLoadingSoal = false
      state.onGoingSoal = payload
      // state.emptyAnswerOnGoingSoal = payload
    },
    restartOnGoingSoal(state){

      // state.onGoingSoal = state.emptyAnswerOnGoingSoal
      // console.log(state.onGoingSoal.data.rows)
      let xm = []
      state.onGoingSoal.data.rows.forEach((element, index) => {
        if (state.onGoingSoal.data.rows[index].answer !== undefined){
          delete state.onGoingSoal.data.rows[index].answer
        }
        // xm.push(element)
      })
      // console.log(state.onGoingSoal.data.rows)
    },
    getCacheJawabanSoal(state){
      // AMBIL CACHE JAWABAN USER DARI CACHE
      if (this.$cookies.get('user-cache-jawaban-soal') !== undefined){
        state.cacheJawabanSoalUser = this.$cookies.get('user-cache-jawaban-soal')
      }
      // this.$cookies.get('user-cache-soal')
    },
    setCacheJawabanSoal(state, payload){
      // SET CACHE JAWABAN USER PADA TEST
      state.cacheJawabanSoalUser.push(payload)
      this.$cookies.set('user-cache-jawaban-soal', state.cacheJawabanSoalUser,  {
        path: '/',
        maxAge: 60 * 60 * 24 * 7
      })
    },
    clearCacheSoal(state){

    },
    setUserScoreCache(state, payload){
      // SET USER SCORE CACHE
      state.userScoreCache = payload
      this.$cookies.set('user-score-cache',state.userScoreCache,  {
        path: '/',
        maxAge: 60 * 60 * 24 * 7
      })
    },
    getUserScoreCache(state){
      // AMBIL DATA SCORE USER DARI CACHE
      state.userScoreCache = this.$cookies.get('user-score-cache')
    },
    setCacheSessionModuleUser(state, payload){

      state.cacheJawabanSoalUser = []
      state.userScoreCache = null
      this.$cookies.remove('user-score-cache')
      this.$cookies.remove('user-cache-jawaban-soal')
      // console.log(state.cacheJawabanSoalUser, state.userScoreCache)
      // SET CACHING UNTUK MODULE NAME DAN TEMA NAME
      // console.log(payload)
      state.userCacheSessionModule = payload
      // state.userCacheSessionModule = {
      //   id_tema: payload.id,
      //   nama_tema: payload.theme_name,
      //   id_module: payload.module_id,
      //   nama_module: payload.matkul,
      //   module_slug: payload.module_slug,
      //   paket_soal_id: payload.selectedPaketSoal.id,
      //   paket_soal_name: payload.selectedPaketSoal.name,
      //   kelas: payload.kelas,
      //   type_test: payload.type,
      //   id_langganan: payload.idSubs
      // }
      // this.$cookies.remove('user-module-cache')
      this.$cookies.set('user-module-cache',  state.userCacheSessionModule,  {
        path: '/',
        maxAge: 60 * 60 * 24 * 7
      })
      // console.log(this.$cookies.get('user-module-cache'))
      // console.log(state.userCacheSessionModule)
    },
    getCacheSessionModuleUser(state){
      // GET CACHING UNTUK MODULE NAME DAN TEMA NAME
      // console.log(this.$cookies.get('user-module-cache'))
      state.userCacheSessionModule = this.$cookies.get('user-module-cache')
      // this.$cookies.remove('user-module-cache')
    },
    setLoadingState(state, payload){
      state.examLoadingSoal = payload
    },
    removeCacheSoal(state){
      // console.log(this.$cookies.getAll())
      // console.log(state.cacheJawabanSoalUser, state.userScoreCache)
      state.cacheJawabanSoalUser = []
      state.userScoreCache = null
      state.onGoingSoal = null
      this.$cookies.remove('user-score-cache')
      this.$cookies.remove('user-cache-jawaban-soal')
    },
    setCachePembahasan(state, payload){
      state.cachePembahasan = payload
    }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
    sample: state => {
        return state.sample
    },
}
