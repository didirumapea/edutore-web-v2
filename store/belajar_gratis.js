// STATE AS VARIABLE
const slugify = require('slugify')
import { dataAccord } from './../data/data';
// console.log(dataAccord)
export const state = () => ({
  accordBelajarOnline: dataAccord,
  embedEducation :  [
    {
      id: 77,
      name: '10',
      education:
        { id: 3,
          education_name: 'SMK',
          status_delete: false,
          created_at: '2019-01-15T01:38:48.000Z',
          updated_at: '2019-01-15T01:38:48.000Z'
        }
    },
    {
      id: 78,
      name: '11',
      education:
        { id: 3,
          education_name: 'SMK',
          status_delete: false,
          created_at: '2019-01-15T01:38:48.000Z',
          updated_at: '2019-01-15T01:38:48.000Z'
        }
    },
    {
      id: 79,
      name: '12',
      education:
        { id: 3,
          education_name: 'SMK',
          status_delete: false,
          created_at: '2019-01-15T01:38:48.000Z',
          updated_at: '2019-01-15T01:38:48.000Z'
        }
    },
  ],
  cacheKelas: [],
  kelas: 0,
  kelasData: [],
  matpel: null,
  setListMatPel: [
    {
      kelas: 0,
      matkul: 'sample'
    }
  ],
  setListModule: [],
  routeParams: null
  })

// ACTIONS AS METHODS
  export const actions = { // asyncronous
    async nuxtServerInit ({commit}) {
      // console.log('from server init')
      // let {data} = await axios.get('employees.json')
      // let {data} = await context.$axios.get('kelas')
      // commit('setCacheKelas', values(data))

    },
    async getKelasMatpel ({ commit }, payload) {
        // return await this.$axios.get(`paket-soal-trial?kelas=${payload.kelas}&mata_pelajaran=${payload.matpel}`)
        return await this.$axios.get(`${process.env.API_URL}/paket-soal-trial?kelas=${payload.kelas}&mata_pelajaran=${payload.matpel}`)
        .then((response) => {
          // console.log(response.data)
          return response.data
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.l og(err.response)
          return err.response
        })
      },
    async setKelas ({ commit }, payload) {
      return await this.$axios.get(`${process.env.API_URL}/kelas`)
        // return await this.$axios.get('kelas')
        .then((response) => {
          // console.log(response.data.data.rows)
          if (payload === null){
            commit('setCacheKelas', response.data.data.rows)
            // console.log(payload, 'on true')
          }else{
            commit('setCacheKelasByJenjang', response.data.data.rows.filter((x) => x.education.education_name.toUpperCase() === payload.toUpperCase().replace('-', '/')))
            // console.log(payload, 'on false')
          }
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.l og(err.response)
          return err.response
        })
    },
    async setKelasByJenjang ({ commit }, payload) {
      await this.$axios.get('api/v1/kelas')
        .then((response) => {
          console.log(response.data.data.rows, payload)
          console.log(payload.toUpperCase().replace('-', '/'))
          console.log(response.data.data.rows.filter((x) => x.education.education_name === 'SD/MI'))
          // commit('setCacheKelasByJenjang', response.data.data.rows)
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.l og(err.response)
          return err.response
        })
    },
  }
// MUTATION AS LOGIC
export const mutations = { // syncronous
  setCacheKelas(state, payload){
    try{
      state.embedEducation.forEach((element) => {
        payload.splice(payload.length - 1, 0, element)
      })
      // console.log(payload)
      let half_length = Math.ceil(payload.length / 2);
      state.cacheKelas = {
        leftSideKelas: payload.splice(0, half_length),
        rightSideKelas: payload.splice(0, half_length)
      }
      // console.log(state.cacheKelas)
    }
    catch (ex){
      console.log('error' + ex)
    }
  },
  setCacheKelasByJenjang(state, payload){
    try{
      // console.log(payload)
      let half_length = Math.ceil(payload.length / 2);
      state.cacheKelas = {
        leftSideKelas: payload.splice(0, half_length),
        rightSideKelas: payload.splice(0, half_length)
      }
    }
    catch (ex){
      console.log('error' + ex)
    }

  },
  setCacheKelas2(state, payload){
    payload.push(payload.push(state.embedEducation))
    // console.log(payload)
  },
  setRouteParams(state, payload){
    payload.slug = slugify(`${payload.matpel} ${payload.kelas_name.replace('/', ' ')} Kelas ${payload.kelas}`, {lower: true})
    let dt = new Date(this.$moment().add(1, 'd').format())
    payload.full_title = `${payload.matpel} ${payload.kelas_name} Kelas ${payload.kelas}`
    payload.jenjang = payload.kelas_name
    this.$cookies.set('url-params', payload, {
      path: '/',
      expires: dt
    })
    state.routeParams = payload
  },
  setKelas(state, payload){
    state.kelas = payload
  },
  setMatpel(state, payload){
    state.matpel = payload
  },
  setDataKelas(state, payload){
    state.kelasData.push(payload)
  },
  setListMatPel(state, payload){
    state.setListMatPel.push(payload)
  },
  setListModule(state, payload){
    let data = {
      matpel: payload.matPel,
      listModule: payload.listData
    }
    // console.log(payload)
    state.setListModule.push(data)
  },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  kelasData: state => {
    return state.kelasData
  },
}
