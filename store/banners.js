//import config from '~/store/config'
import axios from 'axios'
// Set config defaults when creating the instance
// console.log(process.env.NODE_ENV)
// const instance = axios.create({
//   baseURL: process.env.NODE_ENV === 'development' ?
//     'http://localhost:2000/api/v1/' :
//     // 'https://banner.edutore.com/api/v1/':
//     'https://banner.edutore.com/api/v1/' ,
//   headers: {
//     'On-Project': 'edutore-x-2020',
//     'Signature-Key': 'tpsRXbUIl6/iHZ4ZS9gqjz+JnY3qSsr0WESiu6QPRoz0LvU+W+x2drxzi796KcH6',
//   }
// });
// STATE AS VARIABLE
export const state = () => ({
    savedQuestion: [],
    pathImageBanner: process.env.NODE_ENV === 'development' ?
    // 'http://localhost:2000/sub-edutore-files/assets/images/banner/' :
    'https://storage.googleapis.com/edutore-cdn/sub-edutore-files/banners/':
    // 'http://35.190.44.87/sub-edutore-files/banners/':
    // 'https://cdn.edutore.com/sub-edutore-files/banners/':
    // 'https://banner.edutore.com/sub-edutore-files/assets/images/banner/':
    // 'https://banner.edutore.com/sub-edutore-files/assets/images/banner/'
    'https://cdn.edutore.com/sub-edutore-files/banners/'
  })
// ACTIONS AS METHODS
  export const actions = { // asyncronous
    async getBannerBeranda ({ commit }) {
      // return await instance.get('web/banners/list/banner-on=edutore/category=beranda/page=1/limit=10/column-sort=position/sort=asc', )
      return await this.$axios.$get('sub-api/web/banners/list/banner-on=edutore/category=beranda/page=1/limit=10/column-sort=position/sort=asc')
        .then((response) => {
          // console.log(response)
          return response.data
        })
        .catch(err => {
          if (err.response === undefined){
            return 'no connection'
            // console.log('No Connection')
          }else{
            // console.log(err.response)
          }

          //   return err.response
        })
    },
  }
// MUTATION AS LOGIC
export const mutations = { // syncronous
    add (state, text) {
        state.list.push({
        text: text,
        done: false
        })
    },
    remove (state, { todo }) {
        state.list.splice(state.list.indexOf(todo), 1)
    },
    toggle (state, todo) {
        todo.done = !todo.done
    }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
    savedQuestion: state => {
		  return state.savedQuestion
    },
}
