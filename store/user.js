// STATE AS VARIABLE
export const state = () => ({
        dUser: '',
        listProvince: [],
        userSignUpEmail: null,
        myModuleOrder: null,
        myLanggananOrder: null
  })
// ACTIONS AS METHODS
  export const actions = { // asyncronous
    async updateUserProfile ({ commit }, context) {
      // console.log(context)
      let token = this.$cookies.get('user').token
        return await this.$axios.$post('api/v1/profile', context, {headers : {'x-access-token': token}})
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
            return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.l og(err.response)
          return err.response
        })
      },
    async getUserProfile ({ commit }, payload) {
      let token = this.$cookies.get('user').token
      return await this.$axios.get('api/v1/profile/'+payload, {headers : {'x-access-token': token}})
        .then((response) => {
          // console.log(response)
          // commit('set_list_province', response.data.data.rows)
          return response.data
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.log(err.response)
          return err.response
        })
    },
    async getListProvince ({ commit }) {
      // console.log(context)
      return await this.$axios.get('api/v1/province')
        .then((response) => {
          // console.log(response)
          // commit('set_list_province', response.data.data.rows)
          return response.data
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.l og(err.response)
          return err.response
        })
    },
    async getListCity ({ commit }, payload) {
      // console.log(context)
      return await this.$axios.get('api/v1/city/'+payload)
        .then((response) => {
          // console.log(response)
          // commit('set_list_province', response.data.data.rows)
          return response.data
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.l og(err.response)
          return err.response
        })
    },
    async getListDistrict ({ commit }, payload) {
      // console.log(context)
      return await this.$axios.get('api/v1/district/'+payload)
        .then((response) => {
          // console.log(response)
          // commit('set_list_province', response.data.data.rows)
          return response.data
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          // console.l og(err.response)
          return err.response
        })
    },

  }
// MUTATION AS LOGIC
export const mutations = { // syncronous
    SET_EMAIL_SIGNUP(state, data) {
      state.userSignUpEmail = data
      // console.log(data)
    },
    SET_LOGIN(state, data) {
        state.isLogin = data
      },
    set_list_province(state, payload) {
      state.listProvince = payload
      // console.log(state.listProvince)
    },
    set_user (state, data) {
      // state.user = data
      state.user.name = data.displayName
      state.user.email = data.email
      state.user.token = data.stsTokenManager.accessToken
      state.isLogin = true
      // const cookieValObject = { param1: 'value1', param2: 'value2' }
      this.$cookies.set('user', state.user, {
        path: '/',
        maxAge: 60 * 60 * 24 * 7
      })
      this.$cookies.set('user-details', data, {
        path: '/',
        maxAge: 60 * 60 * 24 * 7
      })
      // console.log(state.user)
    },
    reset_user (state) {
      state.user = null
    },
    setMymoduleOrder(state, payload){
      state.myModuleOrder = payload
    },
    setMyLanggananOrder(state, payload){
      state.myLanggananOrder = payload
    }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
  user: state => {
    return state.user
  },
  listProvince: state => {
    return state.listProvince
  },
  userSignUpEmail: state => {
    return state.userSignUpEmail
  },
}
