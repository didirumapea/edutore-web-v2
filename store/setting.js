// let cdnUrl = 'https://cdn.edutore.com/';
let cdnUrl = 'http://35.190.44.87/';
// const storare = require('@google')
if (process.env.NODE_ENV === 'production'){
  cdnUrl = 'https://cdn.edutore.com/'
}

export const state = () => ({
    organizationSchemaFormat: {
      "@context" : "http://schema.org",
      "@type" : "Organization",
      "name" : "Edutore",
      "url" : process.env.BASE_URL,
      "sameAs" : [
        "https://web.facebook.com/edutorecom",
        "https://twitter.com/edutorecom",
        "https://www.instagram.com/edutorecom",
        "https://www.youtube.com/channel/UCKHHM8nVVgOAUovFP8vZ0xA",
      ],
      "logo": "https://news.edutore.com/wp-content/uploads/2019/10/logo-edutore-HiRes.png"
    },
    list: [],
    version: '1.13',
    // image: 'this is image',
    // api: 'http://api.edutore.com/',
    // api: 'http://192.168.1.4:3000/api/v1/',
    basePathNews: 'https://edutore.com/news/',
    api: 'http://localhost:3000/api/v1/',
    // ----------------------- PRODUCTION -------------------------
    cdnBasePathModule: cdnUrl+'public/module/',
    cdnBasePathModuleThumb: cdnUrl+'public/module/thumb/',
    cdnBasePathChoice: cdnUrl+'public/soal/answer_choice/',
    cdnBasePathQuestion: cdnUrl+'public/soal/question/',
    cdnBasePathPublisher: cdnUrl+'public/publisher/thumb/',
    cdnBasePathDiscussion: cdnUrl+'public/soal/discussion/',
    cdnBasePathDiscussionThumb: cdnUrl+'public/soal/discussion/thumb/',
    // ------------------------ DEVELOPMENT ------------------------------------
    // cdnBasePathModuleThumb: 'http://35.190.44.87/public/module/thumb/',
    // cdnBasePathChoice: 'http://35.190.44.87/public/soal/answer_choice/',
    // cdnBasePathQuestion: 'http://35.190.44.87/public/soal/question/',
    // cdnBasePathPublisher: 'http://35.190.44.87/public/publisher/thumb/',
    // cdnBasePathDiscussion: 'http://35.190.44.87/public/soal/discussion/',
    // cdnBasePathDiscussionThumb: 'http://35.190.44.87/public/soal/discussion/thumb/',
    // cdnBasePathModule: 'http://35.190.44.87/public/module/',
  })


// ACTIONS AS METHODS
export const actions = { // asyncronous
  async mailchimpSubscribe ({ commit }, context) {

    // return await this.$axios.$post('api/https://edutore.us20.list-manage.com/subscribe/post', {u: 'fe45aee2f112f4d2a110ad3b0', id: '018e5a3110', MERGE0: context}, {headers: {
    //     'Content-Type': 'application/x-www-form-urlencoded',
    //     'Access-Control-Allow-Origin':'*',
    //     'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, OPTIONS'
    //   }})
    return await this.$axios.$post('api/https://edutore.us20.list-manage.com/subscribe/post?u=fe45aee2f112f4d2a110ad3b0&id=018e5a3110&MERGE0=didirumapea1894@gmail.com')
      .then((response) => {
        console.log(response)
        // commit('set_user', response.data.user)
        return response
      })
      .catch(err => {
        // alert(JSON.stringify(err.response))
        // console.log(err.response)
        return err.response
      })
  },
}

export const mutations = {
    add (state, text) {
        state.list.push({
        text: text,
        done: false
        })
    },
    remove (state, { todo }) {
        state.list.splice(state.list.indexOf(todo), 1)
    },
    toggle (state, todo) {
        todo.done = !todo.done
    }
}

export const getters = {
    image: state => {
		return state.image
    },
    api: state => {
		return state.api
    },
    cdnBasePathModuleThumb: state => {
		return state.cdnBasePathModuleThumb
    },
    cdnBasePathModule: state => {
		return state.cdnBasePathModule
    },
    cdnBasePathChoice: state => {
		return state.cdnBasePathChoice
    },
    cdnBasePathQuestion: state => {
		return state.cdnBasePathQuestion
    },
    cdnBasePathPublisher: state => {
      return state.cdnBasePathPublisher
    },
    cdnBasePathDiscussion: state => {
      return state.cdnBasePathDiscussion
    },
    cdnBasePathDiscussionThumb: state => {
      return state.cdnBasePathDiscussionThumb
    },
}
