

// STATE AS VARIABLE
export const state = () => ({
    list: [],
    product: [],
    count: 0,
    message: 'hello from vuex',
    sessionThema: null,
    sessionSoal: null,
    soal: null,
    score: null,
    sessionPaketSoal: [],
    isLastPaketSoal: false
  })
// ACTIONS AS METHODS
  export const actions = { // asyncronous
    nuxtServerInit ({ commit }, { req }) {
        if (req.session.user) {
          commit('user', req.session.user)
        }
      },
      // THEMA REGION
    setSessionThema(state, payload) {
        state.commit('setSessionThema', payload)
        state.commit('setPaketSoal', payload)
        // console.log(payload)
    },
    getSessionThema({commit}) {
        // console.log(this.$cookies.get('session-thema'))
        if (this.$cookies.get('session-thema') !== undefined){
            commit('getSessionThema', this.$cookies.get('session-thema'))
        }

        // console.log(this.$cookies.get('session-thema'))
    },
    resetSessionThema(state){
        state.commit('resetSessionThema')
    },
    // SOAL REGION
    async setSoal ({commit}, payload) {
        // console.log(payload+' ini set soal')
        let paketSoalId = null

        let data = this.$cookies.get('session-paket-soal')
        // console.log(data)
        for (let x = 0; x < data.length; x++){
          // console.log(data[x])
            if (data[x].status === false){
                paketSoalId = data[x].id
                break;
            }
        }

        // let sessionThema = this.$cookies.get('session-thema')
        let sessionThema = payload
        // let paketSoalId = sessionThema.paket_soals[0].id
        let idModule = sessionThema.module_id
        // console.log(sessionThema)
        // console.log(sessionThema.selectedPaketSoal)
        if (sessionThema.selectedPaketSoal.trial){
          let token = ''
          if (this.$cookies.get('user') !== undefined){
            token = this.$cookies.get('user').token
          }
          // console.log(this.$cookies.get('user'))


          return await this.$axios.$get('api/v1/trials/modules/' + idModule + '/paket_soal/' + sessionThema.selectedPaketSoal.id, {headers: {'x-access-token': token}})
            .then((response) => {
              // console.log(response)
              response.paket_name_now = sessionThema.selectedPaketSoal.name
              response.paketSoalId = sessionThema.selectedPaketSoal.id
              response.isTrial = true
              //console.log(response)
              commit('setSoal', response)
              // state.commit('set_user', response.data.user)
              return response
            })
            .catch(err => {
              // alert(JSON.stringify(err.response))
              console.log(err.response)
              return err.response
            })
        }else if (sessionThema.type === 'langganan'){
          let token = this.$cookies.get('user').token
          return await this.$axios.$get('api/v1/my-subscribe-soal/' + sessionThema.idSubs +'/modules/'+ idModule + '/paket_soal/' + sessionThema.selectedPaketSoal.id, {headers: {'x-access-token': token}})
            .then((response) => {
              // sessionThema.paket_soals.forEach(element => {
                // if (paketSoalId === element.id) {
                  response.paket_name_now = sessionThema.selectedPaketSoal.name
                //   console.log(element)
                // }
              // });
              response.paketSoalId = sessionThema.selectedPaketSoal.id
              response.isTrial = false
              // console.log(sessionThema.selectedPaketSoal)
              commit('setSoal', response)
              // state.commit('set_user', response.data.user)
              return response
            })
            .catch(err => {
              // alert(JSON.stringify(err.response))
              console.log(err.response)
              return err.response
            })
        }else{
          // console.log('new era')
          let token = this.$cookies.get('user').token
          return await this.$axios.$get('api/v1/soals/modules/' + idModule + '/paket_soal/' + sessionThema.selectedPaketSoal.id, {headers: {'x-access-token': token}})
            .then((response) => {
              // sessionThema.paket_soals.forEach(element => {
              //   if (paketSoalId === element.id) {
              //     response.paket_name_now = element.name
              response.paket_name_now = sessionThema.selectedPaketSoal.name
              //     // console.log(element.name)
              //   }
              // });
              response.paketSoalId = paketSoalId
              response.isTrial = false
              // console.log(sessionThema.paket_soals)
              commit('setSoal', response)
              // state.commit('set_user', response.data.user)
              return response
            })
            .catch(err => {
              // alert(JSON.stringify(err.response))
              console.log(err.response)
              return err.response
            })
        }
      },
    async getSessionSoal(state) {
        // console.log(this.$cookies.get('session-thema'))
        if (this.$cookies.get('session-soal') !== undefined){
            // state.commit('getSessionThema', this.$cookies.get('session-thema'))
            return this.$cookies.get('session-soal')
        }
        // console.log(this.$cookies.get('session-thema'))
    },
    // REGION SCORE
    async setScore ({commit}, payload) {
        // console.log(payload)
        let scoreDetails = {
            title: this.$cookies.get('session-thema').matkul,
            subtitle: this.$cookies.get('session-thema').theme_name,
            scoreboard: payload.lastScore,
            totalSoal: payload.noq,
            time: payload.timeElapsed
        }
        commit('setScore', scoreDetails)
        // console.log(this.$cookies.get('session-thema'))
      },
    async getScore ({commit}) {
        commit('getScore')
      },
    async resetScore ({commit}) {
        commit('resetScore')
      },
    async assesment({commit}, payload){
        let token = this.$cookies.get('user').token
        // console.log(payload)
        let data = {
            Assesment: [payload]
        }
        // console.log(data)
        this.$axios.$post('api/v1/assesment', data, {headers : {'x-access-token': token}})
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
        })
        .catch(err => {
            // alert(JSON.stringify(err.response))
            console.log(err.response)
        })
      },
    async laporkanSoal({commit}, payload){
        let token = this.$cookies.get('user').token
        let data = {
          paket_soal_id: payload.paketSoalId,
          soal_id: payload.soalId,
          alasan: payload.alasan
        }
        return await this.$axios.$post('api/v1/report-soal', data, {headers : {'x-access-token': token}})
          .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
            return response
          })
          .catch(err => {
            // alert(JSON.stringify(err.response))
            console.log(err.response)
          })
      }
  }
// MUTATION AS LOGIC
export const mutations = { // syncronous
    // THEMA REGION
    setSessionThema(state, payload){
        // console.log(payload)
        state.sessionThema = payload
        this.$cookies.set('session-thema', payload, {
            path: '/',
            maxAge: 60 * 60 * 24 * 7
          })
    },
    getSessionThema(state, payload){
        state.sessionThema = payload;
    },
    resetSessionThema(state){
        this.$cookies.remove('session-thema')
        state.sessionTestSoal = null;
    },
    // SOAL REGION
    setSoal(state, payload){
        // console.log(payload.data)
        state.soal = payload
        // this.$cookies.set('soal', payload.data, {
        //     path: '/',
        //     maxAge: 60 * 60 * 24 * 7
        //   })
        //   console.log(state.soal)
    },
    getSessionSoal(state, payload){
        state.sessionSoal = payload;
    },

    // SCORE REGION
    setScore(state, payload){
        // console.log(payload.data)
        state.score = payload
        this.$cookies.set('session-score', payload, {
            path: '/',
            maxAge: 60 * 60 * 24 * 7
          })
        //   console.log(state.score)
    },
    getScore(state){
        // console.log(payload.data)
        state.score = this.$cookies.get('session-score')
        // console.log(this.$cookies.get('session-score'))
    },
    // PAKET SOAL REGION
    setPaketSoal(state, payload){
        // console.log(payload.paket_soals)

        payload.paket_soals.forEach(element => {

            let data = {
                id: null,
                status: null
            }
            data.id = element.id
            data.status = false
            state.sessionPaketSoal.push(data)
            this.$cookies.remove('session-paket-soal')
            this.$cookies.set('session-paket-soal', state.sessionPaketSoal, {
                path: '/',
                maxAge: 60 * 60 * 24 * 7
              })
        });
        // console.log(state.sessionPaketSoal)
    },
    updatePaketSoal(state, payload){
        let data = this.$cookies.get('session-paket-soal')
        // console.log(payload)
        for (let x = 0; x < data.length; x++){
            // console.log(data[x].id)
            if (data[x].id.toString() === payload){
                data[x].status = 'done'
                this.$cookies.remove('session-paket-soal')
                this.$cookies.set('session-paket-soal', data, {
                    path: '/',
                    maxAge: 60 * 60 * 24 * 7
                  })
            }
            // console.log(data)

        }
        // console.log(data)
        // console.log(this.$cookies.get('session-paket-soal'))

    },
    checkPaketSoal(state){
        // console.log( this.$cookies.get('session-paket-soal'))
        let data = this.$cookies.get('session-paket-soal')
        let cntPaketSoal = 0
        for (let x = 0; x < data.length; x++){
            if (data[x].status === false){
                cntPaketSoal++
            }
        }
        if (cntPaketSoal > 1){
            state.isLastPaketSoal = false
        }else {
            state.isLastPaketSoal = true
        }
    },
    clearPaketSoal(state){
        // console.log(this.$cookies.remove('session-paket-soal'))
        this.$cookies.remove('session-paket-soal')
        state.sessionPaketSoal = []
    }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
    sessionThema: state => {
        return state.sessionThema
    },
    sessionSoal: state => {
        return state.sessionSoal
    },
    soal: state => {
        return state.soal
    },
    score: state => {
        return state.score
    },
    isLastPaketSoal: state => {
        return state.isLastPaketSoal
    },

}
