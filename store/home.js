//import config from '~/store/config'

// STATE AS VARIABLE
export const state = () => ({
    savedQuestion: [],
    homeModule: [],
    isTabActive: '',
    newsData: ''
  })
// ACTIONS AS METHODS
  export const actions = { // asyncronous
    async getRecomendModules ({ commit }) {
      // console.log(Assesment)
      // this.$axios.setToken(token, false)
      // console.log({Assesment})
      // {email: 'rohmat771@gmail.com', password: 'rahasia123'}
      return await this.$axios.$get('api/v1/recommendations')
        .then((response) => {
          // console.log(response)
          //commit('setModule', response)
          return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))

          if (err.response === undefined){
            return 'no connection'
            // console.log('No Connection')
          }else{
            // console.log(err.response)
          }

          //   return err.response
        })
    },
      async getModules ({ commit }) {
        return await this.$axios.$get('api/v1/modules/page=1/limit=6/sort_by=ASC')
        .then((response) => {
            // console.log(response)
            commit('setModule', response)
            return response
        })
        .catch(err => {
            // alert(JSON.stringify(err.response))

            if (err.response === undefined){
                return 'no connection'
                // console.log('No Connection')
            }else{
                // console.log(err.response)
            }

          //   return err.response
        })
      },
      async getPublisherHome ({ commit }) {
        // console.log(Assesment)
        // this.$axios.setToken(token, false)
        // console.log({Assesment})
        // {email: 'rohmat771@gmail.com', password: 'rahasia123'}
        return await this.$axios.$get('api/v1/publishers')
        .then((response) => {
            // console.log(response)
            // commit('set_user', response.data.user)
            return response
        })
        .catch(err => {
            if (err.response === undefined){
                return 'no connection'
                // console.log('No Connection')
            }else{
                // console.log(err.response)
            }
        })
      },
    async getNews({ commit }){
      return await this.$axios.$get('news-info/feed/json/')
        .then((response) => {
          // console.log(response)
          // commit('setArtikel', response)
          return response
          //
        })
        .catch(err => {
          if (err.response === undefined){
            return {
              success: false,
              message: err,
              info: 'no connection'
            }
            // console.log('No Connection')
          }else{
            console.log(err.response)
          }
        })
      // console.log('server init')
    },
  }
// MUTATION AS LOGIC
export const mutations = { // syncronous
    setArtikel(state, payload){
      state.newsData = payload
    },
    setActiveTab(state, payload){
        // console.log(payload)
        state.isTabActive = payload
    },
    setModule(state, payload){
        // console.log(payload)
        state.homeModule = payload.data.rows
    },
    setSavedQuestion(state, payload) {
        localStorage.setItem('savedQuestion', payload)

    },
    setSavedDataSession (state, payload) {
        localStorage.setItem('savedDataSession', payload)
    },
    getSavedQuestion(state) {
        state.savedQuestion = JSON.parse(localStorage.getItem('savedQuestion'))
    },
    getProduct (state, payload){
        state.product = payload
    },
    increment (state, payload) {
        state.count+=payload
    },
    add (state, text) {
        state.list.push({
        text: text,
        done: false
        })
    },
    remove (state, { todo }) {
        state.list.splice(state.list.indexOf(todo), 1)
    },
    toggle (state, todo) {
        todo.done = !todo.done
    }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
    savedQuestion: state => {
		return state.savedQuestion
    },
    homeModule: state => {
		return state.homeModule
    },
    isTabActive : state => {
        return state.isTabActive
    }

}
