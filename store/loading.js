export const state = () => ({
    text: 'init module',
    loadingState: false,
    cntx: 0,
    initLogin: false
  })

  export const actions = { // asyncronous
    increment(state, payload){
        state.commit('increment', payload)
    },
    async getProduct (state) {
    //   const ip = await this.$axios.$get('api/http://api.edutore.com/main-products')
    //   state.commit('getProduct', ip.data.rows)
    }
  }

export const mutations = { // syncronous
    setTextLoading (state, payload){
      // console.log(payload)
      state.text = payload
    },
    stopLoadingState(state, payload){
      if(payload === 'login'){
        state.initLogin = true
      }
      // console.log(payload)
      state.loadingState = false
    },
    startLoadingState(state){
      state.loadingState = true
    },
    testStateWatcher(state){
      state.cntx += 1
    },
    increment (state, payload) {
        state.count+=payload
    },
    remove (state, { todo }) {
        state.list.splice(state.list.indexOf(todo), 1)
    },
    toggle (state, todo) {
        todo.done = !todo.done
    }
}

export const getters = {
    image: state => {
		return state.image
    },
    product: state => {
		return state.product
    },
    message: state => {
        return state.message
    },
    count: state => {
        return state.count
    }
}
