export const state = () => ({
    list: [],
    product: [],
    count: 0,
    message: 'hello from vuex'
  })

  export const actions = { // asyncronous
    increment(state, payload){
        state.commit('increment', payload)
    },
    async getProduct (state) {
    //   const ip = await this.$axios.$get('api/http://api.edutore.com/main-products')
    //   state.commit('getProduct', ip.data.rows)
    }
  }

export const mutations = { // syncronous
    getProduct (state, payload){
        state.product = payload
    },
    increment (state, payload) {
        state.count+=payload
    },
    add (state, text) {
        state.list.push({
        text: text,
        done: false
        })
    },
    remove (state, { todo }) {
        state.list.splice(state.list.indexOf(todo), 1)
    },
    toggle (state, todo) {
        todo.done = !todo.done
    }
}

export const getters = {
    image: state => {
		return state.image
    },
    product: state => {
		return state.product
    },
    message: state => {
        return state.message
    },
    count: state => {
        return state.count
    }
}
