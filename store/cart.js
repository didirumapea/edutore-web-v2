import firebase, {auth} from '~/services/fireinit'

// STATE AS VARIABLE
export const state = () => ({
    cart: [],
    totalPriceCart: 0,
    totalCart: 0,
    totalCart2: 0,
    listcart: [],
    cartLangganan: [],
    orderCode: null,
    orderExpired: null
  })
// ACTIONS AS METHODS
  export const actions = { // asyncronous
    async buyModule({commit}, payload) {
      commit('buyModule', payload)
    },
    async getOrderModule({commit}) {
      commit('getOrderModule')
    },
    async hapusSelectedCart({commit}, payload) {
      commit('hapusSelectedCart', payload)
    },
    async checkVoucher({commit}, payload) {
      let token = this.$cookies.get('user').token
      // console.log(payload)

      // console.log(data)
     return await this.$axios.post('api/v1/vouchers', payload,{headers : {'x-access-token': token}})
          .then((response) => {
              // console.log(response)
              // commit('set_user', response.data.user)
              return response
          })
          .catch(err => {
            // alert(JSON.stringify(err.response))
          //   console.log(err.response)
            return err.response
        })
      // commit('hapusSelectedCart', payload)
    },
    async checkVoucher2({commit}, payload) {
        return await this.$axios.post('api/v1/vouchers', {voucher: payload})
          .then((response) => {
            // console.log(response.data)
            // commit('set_user', response.data.user)
            return response.data
          })
          .catch(err => {
            // alert(JSON.stringify(err.response))
            //   console.log(err.response)
            return err.response.data
          })
        // commit('hapusSelectedCart', payload)
      },
    async addModuleToDb({commit}, payload) {
      let token = this.$cookies.get('user').token
      let data = {
          module: payload.modules,
          voucher_code: payload.voucherCode
      }
      // console.log(data)
     return await this.$axios.$post('api/v1/order', data, {headers : {'x-access-token': token}})
          .then((response) => {
              // console.log(response)
              // commit('set_user', response.data.user)
              return response
          })
          .catch(err => {
            // alert(JSON.stringify(err.response))
          //   console.log(err.response)
          //    console.log('hellos')
            return err.response
        })
      // commit('hapusSelectedCart', payload)
    },
    async orderViaToko({commit}, payload) {
      let token = this.$cookies.get('user').token
      let data = {
        module: payload.modules,
        voucher_code: payload.voucherCode
      }
      // console.log(data)
      return await this.$axios.$post('api/v1/order/toko', data, {headers : {'x-access-token': token}})
        .then((response) => {
          // console.log(response)
          commit('set_order_code', response.data.id)
          commit('set_order_expired', response.data.expire_at)
          commit('set_total_price', payload.totalPrice)
          return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
            console.log(err.response)
          return err.response
        })
      // commit('hapusSelectedCart', payload)
    },
    async orderViaTokoLangganan({commit}, payload) {
      let token = this.$cookies.get('user').token

      let data = {
        package_subscription: payload.details,
        payment_method: 'TOKO',
        voucher_code: payload.voucher_code
      }
      // console.log(data)
      return await this.$axios.$post('api/v1/order-subscriptions', data, {headers : {'x-access-token': token}})
        .then((response) => {

          // console.log(response)
          commit('clearCart')
          commit('set_order_expired', response.data.expire_at)
          commit('set_cart_langganan', response.data)
          return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          //   console.log(err.response)
          return err.response
        })
      // commit('hapusSelectedCart', payload)
    },
    async orderViaMidtransLangganan({commit}, payload) {

      let token = this.$cookies.get('user').token
      //console.log(payload)

      let data = {
        package_subscription: payload.details,
        payment_method: 'MIDTRANS',
        voucher_code: payload.voucher_code
      }
      // console.log(data)
      return await this.$axios.$post('api/v1/order-subscriptions', data, {headers : {'x-access-token': token}})
        .then((response) => {

          // console.log(response)
          commit('set_cart_langganan', response.data)
          return response
        })
        .catch(err => {
          // alert(JSON.stringify(err.response))
          console.log(err.response)
          return err.response
        })
      // commit('hapusSelectedCart', payload)
    },
    async clearCart({commit}){
        commit('clearCart')
    },
    async clearCartLangganan({commit}){
        commit('clearCartLangganan')
      }
}
// MUTATION AS LOGIC
export const mutations = { // syncronous
    updateTotalCart(state, payload){
      state.totalCart2 += payload
      // console.log(state.totalCart2)
    },
    set_cart_langganan(state, payload){
      state.cartLangganan = payload;
      this.$cookies.set('langganan-order', state.cartLangganan, {
        path: '/',
        maxAge: 60 * 60 * 24 * 7
      })
    },
    set_order_code(state, payload){
        // console.log(payload)

      state.orderCode = payload;
      this.$cookies.set('order-code', state.orderCode, {
        path: '/',
        maxAge: 60 * 60 * 24 * 7
      })
    },
    set_order_expired(state, payload){
      // console.log(payload)

      state.orderExpired = payload;
      this.$cookies.set('order-expired', state.orderExpired, {
        path: '/',
        maxAge: 60 * 60 * 24 * 7
      })
    },
    set_total_price(state, payload){
      // console.log(payload)

      state.totalPriceCart = payload;
      this.$cookies.set('total-price', state.totalPriceCart, {
        path: '/',
        maxAge: 60 * 60 * 24 * 7
      })
    },
    get_total_price(state){
      // console.log(payload)
      state.totalPriceCart = this.$cookies.get('total-price');
    },
    buyModule(state, payload){
        // console.log(payload)

        if (this.$cookies.get('module-order') === undefined){

        }else{
            state.cart = this.$cookies.get('module-order')
        }
        let modules = {
            moduleId: payload.id,
            name: payload.name,
            description: payload.publisher.name,
            image: payload.image,
            price: payload.price,
            catId: payload.module_category_id
        }
        state.cart.push(modules)
        this.$cookies.set('module-order', state.cart, {
            path: '/',
            maxAge: 60 * 60 * 24 * 7
          })
        state.totalCart = state.cart.length
          // console.log(state.cart.length, state.cart)
    },
    getOrderModule(state) {
        if (this.$cookies.get('module-order') !== undefined){
            state.totalCart = this.$cookies.get('module-order').length
            state.cart = this.$cookies.get('module-order')
        }else{
            state.totalCart = 0

        }

    },
    getOrderLangganan(state) {
      if (this.$cookies.get('langganan-order') !== undefined){
        // state.cart = this.$cookies.get('langganan-order').length
        state.cartLangganan = this.$cookies.get('langganan-order')
      }else{
        // state.totalCart = 0

      }

  },
    getOrderCode(state) {
      if (this.$cookies.get('order-code') !== undefined){
        state.orderCode = this.$cookies.get('order-code')
      }else{
        state.orderCode = null
      }

    },
    getOrderExpired(state) {
      if (this.$cookies.get('order-expired') !== undefined){
        state.orderExpired = this.$cookies.get('order-expired')
      }else{
        state.orderExpired = null
      }

    },
    hapusSelectedCart(state, payload) {

         state.cart.splice(payload, 1)
         this.$cookies.remove('module-order')
         this.$cookies.set('module-order', state.cart, {
            path: '/',
            maxAge: 60 * 60 * 24 * 7
          })
      state.totalCart = state.cart.length
            // console.log(this.$cookies.get('module-order'))
            // console.log(fuck)
            // console.log(state.cart)
    },
    clearCart(state){
        this.$cookies.remove('module-order')
        this.$cookies.remove('order-code')
        this.$cookies.remove('order-expired')
        this.$cookies.remove('total-price')
        state.cart = []
        state.totalCart = 0,
        state.listCart = [],
        state.orderCode = null
        state.orderExpired = null
        // commit('clearCart')
    },
    clearCartLangganan(state){
      this.$cookies.remove('langganan-order')
      this.$cookies.remove('order-code')
      state.cartLangganan = []
      state.orderCode = null
      //state.totalCart = 0,
      //  state.listCart = []
      // commit('clearCart')
    }
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {
    totalCart: state => {
        return state.totalCart
    },
    totalPriceCart: state => {
      return state.totalPriceCart
    },
    cart: state => {
        return state.cart
    },
    cartLangganan: state => {
      return state.cartLangganan
    },
    orderCode: state => {
      return state.orderCode
    },
    orderExpired: state => {
      return state.orderExpired
    },
}
