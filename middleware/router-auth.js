
export default async function ({store, redirect, route, app}) {
  // console.log(route.name)
  store.commit('auth/check_login')
  if (store.state.auth.isLogin) {
      // console.log(app.$cookies.get('session-token'))
      // console.log(app.$cookies.get('user'))
    if (app.$cookies.get('session-token') === undefined) {
      await store.dispatch('auth/refreshToken')
    }
  }

  if (store.state.auth.isLogin && (route.name === 'login' || route.name === 'login-password' || route.name === 'daftar')) {
    // console.log(store.commit('auth/checkUser'))
    // console.log(store.commit)
    return redirect('/')
  }
  if (route.name === 'checkout') {
    store.commit('cart/checkItem')
    // console.log('checout route')
    // console.log(store.commit('auth/checkUser'))
    // console.log(store.commit)
    // return redirect('/')
  }
  if (!store.state.auth.isLogin && route.name === 'users-my-subscriptions-details-id') {
    return redirect('/')
  }
  if (route.name === 'belajar-gratis-matpel_kelas') {
    if (app.$cookies.get('url-params') === undefined) {
      return redirect('/belajar-gratis')
    }
  }
  if (route.name === 'tests-tipe_test-id_module-id_tema-id_paket_soal') {
    // console.log(route.params)
    // console.log(route.query.s)
    if (route.query.s === undefined){
      return redirect(`/tests/${route.params.tipe_test}/${route.params.id_module}/${route.params.id_tema}/${route.params.id_paket_soal}?s=1`)
    }
    // Router.push('/')
    // app.router.push('/')

  }
  if (route.name === 'belajar-gratis-latihan-soal-gratis') {
    return redirect('/belajar-gratis/latihan-soal-gratis/sd-mi')
  }
  // console.log(route.name)
  if (!store.state.auth.isLogin && route.name === 'users') {
    return redirect('/login')
  }
  // if (isLogin(store) && route.name === 'login') {
  //   // console.log(store)
  //   return redirect('/admin')
  // }
//
//   if (!isLogin(store) && isAdminRoute(route)) {
//     // console.log(store)
//     return redirect('/login')
//   }
// }
//
// const isLogin = (store) => {
//   // console.log(store)
//   return (store && store.state && store.state.user)
// }
//
// const isAdminRoute = (route) => {
//   //   console.log(route)
//   if (route.matched.some(record => record.path === '/admin')) {
//     return true
//   }
}
