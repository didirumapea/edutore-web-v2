const pkg = require('./package')
const webpack = require("webpack");
require('dotenv').config();
const axios = require('axios')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: ' %s - Edutore by Gramedia',//pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Belajar Latihan soal Gratis, Download App Edutore by Gramedia, Belajar Mandiri dengan Online Learning' },
      { name: 'keywords', content: 'Belajar Gratis Online dengan Aplikasi Edutore by Gramedia' },
      { name: 'author', content: 'Edutore by Gramedia' },
      { name: 'publisher', content: 'Edutore by Gramedia' },
      { name: 'robots', content: 'index, follow' },
      { name: 'language', content: 'id' },
      { name: 'geo.country', content: 'id' },
      { name: 'content-language', content: 'ln-Id' },
      { name: 'geo.placename', content: 'indonesia' },
      { name: 'yandex-verification', content: 'd676c02336e4384c' },
    ],
    htmlAttrs: {
      lang: 'id-ID',
    },
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://use.fontawesome.com/releases/v5.7.1/css/all.css',
      },
      {
        rel: 'stylesheet',
        href:
          'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css',
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/icon?family=Material+Icons',
      }
    ],
    script: [
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'},
      { src: 'https://securepubads.g.doubleclick.net/tag/js/gpt.js', async: true },
    ],
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#557CBF', height: '3px' },
  // loading: '~/components/loader/ripple.vue',
  // loading: [
  //   '~/components/loading.vue',
  //   { color: '#FF0000', height: '3px' }
  // ],

  /*
  ** Global CSS
  */
  css: [
    '~/assets/main.css',
    '@/assets/css/bootstrap.min.css',
    '@/assets/dist/css/swiper.min.css',
    '@/assets/dist/css/swiper.css',
    // '@/assets/css/bootstrap.css',
    // "@/node_modules/bootstrap/dist/css/bootstrap.css",

    '@/assets/css/styles.css',
    // '@/assets/css/modul_bariq.css',
    // '@/assets/css/fontawesome.css',
    // '@/assets/css/fa2.css',
    '@/assets/css/responsive.css',
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '~/plugins/gtm.js', ssr: false },
    { src: '~/plugins/localStorage.js', ssr: false },
    { src: '~/plugins/vue-loading-overlay.js', ssr: true },
    { src: '~/plugins/vue-fab.js', ssr: true },
    { src: '~/plugins/json-id.js', ssr: false},
    // ['~/plugins/json-id.js'],
    // { src: '~plugins/countUp.js', ssr: false },
    '@/plugins/popper',
    '@/plugins/bootstrap',
    { src: '~/plugins/vue-js-modal', ssr: false},
    '~/plugins/axios',
    { src: '~/plugins/swiper.js', ssr: false },
    '~/plugins/func',
    { src: '~plugins/vee-validate.js', ssr: false },
    { src: '~plugins/vue-pagination.js', ssr: false },
    { src: '~plugins/vue-transition.js', ssr: false },
    { src: '~plugins/vue-good-table', ssr: false },
    { src: '~plugins/alexa-metrixs.js', ssr: false },
    // { src: '~/plugins/google-adsense', ssr: false }
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // '@ax2/gpt-ads-module',
    // ['@nuxtjs/google-adsense']
    // ['nuxt-session'],
    ['vue-scrollto/nuxt', { duration: 300 }],
    ['@nuxtjs/sitemap'],
    ['@nuxtjs/dotenv'],
    '@nuxtjs/moment',
    // Simple usage
    // 'nuxt-facebook-pixel-module',

    // With options
    // ['nuxt-facebook-pixel-module', {
    //   /* module options */
    //   track: 'PageView',
    //   pixelId: '1355315167958492',
    //   disabled: false
    // }],
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    // '@nuxtjs/feed',
    // '@nuxtjs/auth',
    '@nuxtjs/toast',
    'cookie-universal-nuxt',
    ['@nuxtjs/google-tag-manager', {
      id: 'GTM-MS3M7VG',
      layer: 'dataLayer',
      pageTracking: true,
      dev: false, // set to false to disable in dev mode
      // query: {
      //   // query params...
      //   gtm_auth:        '...',
      //   gtm_preview:     '...',
      //   gtm_cookies_win: '...'
      // }
    }],
    // Simple usage
    // '@nuxtjs/proxy',

    // With options
    // ['@nuxtjs/proxy', { pathRewrite: { '^/api' : '/api/v1' } }]
  ],
  // gptAds: {
  //   networkCode: 21860443230,
  //   individualRefresh: true,
  //   componentName: 'eduGoogleAds'
  // },
  sitemap: {
    // path: '/sitemapx.xml',
    hostname: 'https://edutore.com',
    // hostname: 'http://localhost:8000',
    exclude: [
      '/modules_backup',
      '/modules_backup/**',
      '/enter-developer-mode'
    ],
    routes: async () => {
      axios.defaults.headers.common = {
        'On-Project': 'edutore-x-2020',
        'Signature-Key': 'tpsRXbUIl6/iHZ4ZS9gqjz+JnY3qSsr0WESiu6QPRoz0LvU+W+x2drxzi796KcH6',
      };
      let { data } = await axios.get(`${process.env.API_URL2}web/sitemap/list/modules/page=1/limit=1000/column-sort=module_slug/sort=asc`)
      return data.data.map(v => `/modules/${v.module_slug}`)
    },
    gzip: true,
    defaults: {
      changefreq: 'daily',
      priority: 1,
      lastmod: new Date(),
      lastmodrealtime: true
    }
  },
  // 'google-adsense': {
  //   id: 'ca-pub-2311798948770569',
  // },
  env: {
    baseUrl: process.env.BASE_URL || 'https://staging.edutore.com',
    API_URL: 'https://api.edutore.com/api/v1',
    MY_COMB: 'hellow from env',
  },
  // feed: [
  //   // A default feed configuration object
  //   {
  //     path: 'https://edutore.com/news/feed/', // The route to your feed.
  //     async create (feed) {
  //       feed.options = {
  //         title: 'Edutore Blog',
  //         link: `https://edutore.com/news/feed/`,
  //         description: "This is Edutore personal feed!"
  //       }
  //
  //       const posts = await client.getEntries({
  //         'content_type': process.env.CTF_BLOG_POST_TYPE_ID
  //       })
  //
  //       posts.items.forEach(post => {
  //         feed.addItem({
  //           title: post.fields.title,
  //           id: post.fields.slug,
  //           link: `${process.env.BASE_URL}/posts/${post.fields.slug}`,
  //           description: post.fields.description,
  //           content: post.fields.description,
  //           date: new Date(post.fields.publishDate),
  //           image: post.fields.heroImage.fields.file.url
  //         })
  //       })
  //
  //       feed.addCategory('Tech')
  //
  //       feed.addContributor({
  //         name: 'Nakamu',
  //         email: 'yuuki.nakamura.0828@gmail.com',
  //         link: process.env.BASE_URL
  //       })
  //     }, // The create function (see below)
  //     cacheTime: 1000 * 60 * 15, // How long should the feed be cached
  //     type: 'rss2', // Can be: rss2, atom1, json1
  //     data: ['Some additional data'] //will be passed as 2nd argument to `create` function
  //   }
  // ],
  axios: {
    // ---------- maunual setting ---------
    proxyHeaders: false,
    // credentials: false,
    // baseURL: process.env.NODE_ENV !== "production"
    //           ? `https://api.edutore.com/`
    //           : "https://api.edutore.com/",
    // ---------- proxy setting -----------
    proxy: true, // Can be also an object with default options
    // prefix: '/api/' // add prefix default API PROXY
  },
  proxy: {
    '/api/v1/': {
      target: process.env.API_URL,
      pathRewrite: {'^/api/v1/': '' }
    },
    '/sub-api/': {
      target: process.env.API_URL2,
      pathRewrite: {'^/sub-api/': '' }
    },
    '/news-info/': {
      target: process.env.NEWS_PATH,
      pathRewrite: {'^/news-info/': '' }
    },
    '/get-token/': {
      target: process.env.GET_REFRESH_TOKEN,
      pathRewrite: {'^/get-token/': '' }
    },
  },
  /*
  ** Nuxt.js toast
  */
 toast: {
  position: 'bottom-right',
  duration: 3000
},

  server: {
    // port: 8000, // default: 3000
    port: process.env.PORT_WEB, // default: 3000
    host: '0.0.0.0', // default: localhost,
  },
  /*
  ** Build configuration
  */
  build: {
    transpile: [
      "vee-validate/dist/rules"
    ],
    /*
    ** You can extend webpack config here
    */
    // vendor: ["jquery", "bootstrap"],
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery'
      })
    ],
    // extend(config, { isDev, isClient }) {
    //   if (isDev && isClient) {
    //     config.module.rules.push({
    //       enforce: "pre",
    //       test: /\.(js|vue)$/,
    //       // loader: "eslint-loader",
    //       exclude: /(node_modules)/
    //     });
    //   }
    // }
    extend(config, ctx) {

    }
  },
  generate: {
    dir: "public"
  }
  // router: {
  //   middleware: ['router-auth']
  // },
}
