var jq = jQuery.noConflict();
jq(document).ready(function(){
   
    jq('footer').load('footer.html');    
  
    
    var swiper = new Swiper('.main-banner, .sec-banner', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      pagination: {
        el: '.swiper-pagination',
      },
    });

    
    jq('.mobile-menu').click(function () { 
      jq('.left-menu').toggleClass('show');
    });
    jq('.close-left').click(function () { 
      jq('.left-menu').removeClass('show');
    });
    jq('#search-btns').click(function () { 
      jq('.search-item').addClass('show');
      jq('.item-search').toggleClass('show');
      
    });
    jq('.search-icos').click(function () { 
      jq('.search-item').removeClass('show');
      jq('.item-search').removeClass('show');
      
    });


    
});