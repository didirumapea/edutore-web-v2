export default {
  data: {
      "version": "https://jsonfeed.org/version/1",
      "user_comment": "This feed allows you to read the posts from this site in any feed reader that supports the JSON Feed format. To add this feed to your reader, copy the following URL -- https://edutore.com/news/feed/json -- and add it your reader.",
      "home_page_url": "https://edutore.com/news",
      "feed_url": "https://edutore.com/news/feed/json",
      "title": "Edutore News",
      "description": "Semua Bisa Pintar",
      "icon": "https://news.edutore.com/wp-content/uploads/2019/04/cropped-favicon-1.png",
      "items": [
        {
          "id": "https://edutore.com/news/mau-kuliah-ke-luar-negeri-belajar-dulu-academic-english/",
          "url": "https://edutore.com/news/mau-kuliah-ke-luar-negeri-belajar-dulu-academic-english/",
          "title": "Mau Kuliah ke Luar Negeri? Belajar dulu Academic English",
          "content_html": "Melanjutkan kuliah ke luar negeri? Bisa jadi merupakan harapan bagi banyak pelajar di Indonesia. Sebagian yang beruntung bisa mewujudkannya dengan dukungan finansial dari orang tua, sebagian lain mengupayakannya lewat beasiswa. Namun dukungan finansial orang tua atau beasiswa tidak serta-merta mampu &#8230; ",
          "content_text": "Melanjutkan kuliah ke luar negeri? Bisa jadi merupakan harapan bagi banyak pelajar di Indonesia. Sebagian yang beruntung bisa mewujudkannya dengan dukungan finansial dari orang tua, sebagian lain mengupayakannya lewat beasiswa. Namun dukungan finansial orang tua atau beasiswa tidak serta-merta mampu &#8230;",
          "date_published": "2019-11-25T13:09:20+07:00",
          "date_modified": "2019-11-28T13:44:37+07:00",
          "author": {
            "name": "admin",
            "url": "https://edutore.com/news/author/admin/",
            "avatar": "https://secure.gravatar.com/avatar/9c7b35983be6e55b6022a26d9fafe621?s=512&d=mm&r=g"
          },
          "tags": [
            "Bahasa Inggris"
          ]
        },
        {
          "id": "https://edutore.com/news/revolusi-teknologi-dan-evolusi-pendidikan/",
          "url": "https://edutore.com/news/revolusi-teknologi-dan-evolusi-pendidikan/",
          "title": "Revolusi Teknologi dan Evolusi Pendidikan",
          "content_html": "&#8220;Evolusi adalah proses perubahan secara berangsur-angsur (bertingkat) di mana sesuatu berubah menjadi bentuk lain (yang biasanya) menjadi lebih kompleks/ rumit ataupun berubah menjadi bentuk yang lebih baik.&#8221; (https://id.m.wikipedia.org/wiki/Evolusi_(istilah)). Salah satu penyebab evolusi adalah adaptasi. Adaptasi adalah kemampuan untuk menyesuaikan diri &#8230; ",
          "content_text": "&#8220;Evolusi adalah proses perubahan secara berangsur-angsur (bertingkat) di mana sesuatu berubah menjadi bentuk lain (yang biasanya) menjadi lebih kompleks/ rumit ataupun berubah menjadi bentuk yang lebih baik.&#8221; (https://id.m.wikipedia.org/wiki/Evolusi_(istilah)). Salah satu penyebab evolusi adalah adaptasi. Adaptasi adalah kemampuan untuk menyesuaikan diri &#8230;",
          "date_published": "2019-11-25T13:08:40+07:00",
          "date_modified": "2019-11-28T13:44:51+07:00",
          "author": {
            "name": "admin",
            "url": "https://edutore.com/news/author/admin/",
            "avatar": "https://secure.gravatar.com/avatar/9c7b35983be6e55b6022a26d9fafe621?s=512&d=mm&r=g"
          },
          "tags": [
            "Sains dan Teknologi"
          ]
        },
        {
          "id": "https://edutore.com/news/memilih-pendidikan-bahasa-inggris-yang-tepat/",
          "url": "https://edutore.com/news/memilih-pendidikan-bahasa-inggris-yang-tepat/",
          "title": "Memilih Pendidikan Bahasa Inggris yang Tepat",
          "content_html": "Pernahkah Anda mendengar atau membaca kalimat dalam bahasa Inggris seperti: &#8220;Argh, I feel boring.” Kalimat yang terucap atau tertulis adalah kata-kata dalam bahasa Inggris, namun ternyata penggunaan kata boring pada kalimat tadi, tidak tepat. Sebab boring bermakna “membosankan”. Kalau yang &#8230; ",
          "content_text": "Pernahkah Anda mendengar atau membaca kalimat dalam bahasa Inggris seperti: &#8220;Argh, I feel boring.” Kalimat yang terucap atau tertulis adalah kata-kata dalam bahasa Inggris, namun ternyata penggunaan kata boring pada kalimat tadi, tidak tepat. Sebab boring bermakna “membosankan”. Kalau yang &#8230;",
          "date_published": "2019-11-25T13:07:27+07:00",
          "date_modified": "2019-11-28T13:45:03+07:00",
          "author": {
            "name": "admin",
            "url": "https://edutore.com/news/author/admin/",
            "avatar": "https://secure.gravatar.com/avatar/9c7b35983be6e55b6022a26d9fafe621?s=512&d=mm&r=g"
          },
          "tags": [
            "Bahasa Inggris"
          ]
        },
        {
          "id": "https://edutore.com/news/simple-past-tense/",
          "url": "https://edutore.com/news/simple-past-tense/",
          "title": "Materi Simple Past Tense – Pengertian, Rumus, Contoh Soal",
          "content_html": "Dalam mempelajari Bahasa Inggris, tenses merupakan basic grammar yang berkaitan dengan waktu. Tenses terdiri dari 16, yang dibagi menjadi 3 berdasarkan rentang waktu yaitu, simple present tense untuk menceritakan kejadian sekarang ini, simple past tense untuk menceritakan kejadian masa lalu &#8230; ",
          "content_text": "Dalam mempelajari Bahasa Inggris, tenses merupakan basic grammar yang berkaitan dengan waktu. Tenses terdiri dari 16, yang dibagi menjadi 3 berdasarkan rentang waktu yaitu, simple present tense untuk menceritakan kejadian sekarang ini, simple past tense untuk menceritakan kejadian masa lalu &#8230;",
          "date_published": "2019-11-21T11:36:44+07:00",
          "date_modified": "2019-11-28T11:05:27+07:00",
          "author": {
            "name": "Feby",
            "url": "https://edutore.com/news/author/feby-dyastika/",
            "avatar": "https://secure.gravatar.com/avatar/a5d22df5938094e29139aa4de938dfc9?s=512&d=mm&r=g"
          },
          "tags": [
            "Bahasa Inggris"
          ]
        },
        {
          "id": "https://edutore.com/news/simple-present-tense/",
          "url": "https://edutore.com/news/simple-present-tense/",
          "title": "Materi Simple Present Tense – Pengertian, Rumus, Contoh Soal",
          "content_html": "Edufriends, kamu suka bingung nggak tentang materi tenses Bahasa Inggris? Hm, kelihatannya sih mudah banget ya, tapi kok ketika soal tenses muncul saat ujian, kamu auto bingung sambil ingat-ingat rumusnya. “Eh beneran nggak yah to be nya ini”  “Aduh kalo &#8230; ",
          "content_text": "Edufriends, kamu suka bingung nggak tentang materi tenses Bahasa Inggris? Hm, kelihatannya sih mudah banget ya, tapi kok ketika soal tenses muncul saat ujian, kamu auto bingung sambil ingat-ingat rumusnya. “Eh beneran nggak yah to be nya ini”  “Aduh kalo &#8230;",
          "date_published": "2019-11-20T12:11:20+07:00",
          "date_modified": "2019-11-21T11:38:39+07:00",
          "author": {
            "name": "Feby",
            "url": "https://edutore.com/news/author/feby-dyastika/",
            "avatar": "https://secure.gravatar.com/avatar/a5d22df5938094e29139aa4de938dfc9?s=512&d=mm&r=g"
          },
          "tags": [
            "Bahasa Inggris"
          ]
        },
        {
          "id": "https://edutore.com/news/materi-persamaan-kuadrat/",
          "url": "https://edutore.com/news/materi-persamaan-kuadrat/",
          "title": "Materi Persamaan Kuadrat – Rumus, Akar, & Contoh Soal",
          "date_published": "2019-11-19T14:05:38+07:00",
          "date_modified": "2019-11-20T14:12:41+07:00",
          "author": {
            "name": "admin",
            "url": "https://edutore.com/news/author/admin/",
            "avatar": "https://secure.gravatar.com/avatar/9c7b35983be6e55b6022a26d9fafe621?s=512&d=mm&r=g"
          },
          "tags": [
            "Matematika"
          ]
        },
        {
          "id": "https://edutore.com/news/perbedaan-jam-belajar-sekolah-di-negara-lain/",
          "url": "https://edutore.com/news/perbedaan-jam-belajar-sekolah-di-negara-lain/",
          "title": "Perbedaan Jam Belajar Sekolah di Negara Lain",
          "content_html": "Jam belajar sekolah di tiap negara tentu berbeda. Ada yang memiliki jumlah jam belajar paling lama dan paling sebentar. Jumlah jam belajar tersebut biasanya ditentukan berdasarkan kebutuhan dari sistem pendidikan yang telah ditetapkan oleh negara tersebut. Jam belajar di negara &#8230; ",
          "content_text": "Jam belajar sekolah di tiap negara tentu berbeda. Ada yang memiliki jumlah jam belajar paling lama dan paling sebentar. Jumlah jam belajar tersebut biasanya ditentukan berdasarkan kebutuhan dari sistem pendidikan yang telah ditetapkan oleh negara tersebut. Jam belajar di negara &#8230;",
          "date_published": "2019-09-30T09:56:26+07:00",
          "date_modified": "2019-09-30T09:56:26+07:00",
          "author": {
            "name": "Feby",
            "url": "https://edutore.com/news/author/feby-dyastika/",
            "avatar": "https://secure.gravatar.com/avatar/a5d22df5938094e29139aa4de938dfc9?s=512&d=mm&r=g"
          },
          "image": "https://news.edutore.com/wp-content/uploads/2019/09/jam-belajar_main_edunews.jpg",
          "tags": [
            "china",
            "finlandia",
            "indonesia",
            "jam belajar",
            "jepang",
            "korea selatan",
            "singapura",
            "Feature"
          ]
        },
        {
          "id": "https://edutore.com/news/inilah-asal-mula-adanya-program-kkn-kuliah-kerja-nyata/",
          "url": "https://edutore.com/news/inilah-asal-mula-adanya-program-kkn-kuliah-kerja-nyata/",
          "title": "Gak Selalu Horror, Inilah Asal Mula Program KKN (Kuliah Kerja Nyata)",
          "content_html": "Beberapa waktu lalu sempat viral banget cerita mistis tentang pengalaman KKN. Cerita yang awalnya disampaikan dalam sebuah thread di twitter itu pun menarik perhatian warganet hingga menyebar luas. Cerita bertajuk KKN di Desa Penari tersebut kemudian memancing pembaca untuk ikut &#8230; ",
          "content_text": "Beberapa waktu lalu sempat viral banget cerita mistis tentang pengalaman KKN. Cerita yang awalnya disampaikan dalam sebuah thread di twitter itu pun menarik perhatian warganet hingga menyebar luas. Cerita bertajuk KKN di Desa Penari tersebut kemudian memancing pembaca untuk ikut &#8230;",
          "date_published": "2019-09-26T11:01:39+07:00",
          "date_modified": "2019-11-04T10:44:17+07:00",
          "author": {
            "name": "Feby",
            "url": "https://edutore.com/news/author/feby-dyastika/",
            "avatar": "https://secure.gravatar.com/avatar/a5d22df5938094e29139aa4de938dfc9?s=512&d=mm&r=g"
          },
          "image": "https://news.edutore.com/wp-content/uploads/2019/09/KKN-COVER2.jpg",
          "tags": [
            "desa penari",
            "kkn",
            "kuliah kerja nyata",
            "Sosial Budaya"
          ]
        },
        {
          "id": "https://edutore.com/news/mengapa-kopi-instan-mudah-terbakar/",
          "url": "https://edutore.com/news/mengapa-kopi-instan-mudah-terbakar/",
          "title": "Mengapa Kopi Instan Mudah Terbakar?",
          "content_html": "Edufriends, pernah mendengar atau melihat video tentang salah satu merek kopi kemasan yang membuat api membesar saat ditaburkan ke atas api? Gara-gara video viral tersebut, kopi kemasan sempat diduga menggunakan bubuk mesiu dalam komposisi kopinya. Wah kira-kira benar nggak ya? &#8230; ",
          "content_text": "Edufriends, pernah mendengar atau melihat video tentang salah satu merek kopi kemasan yang membuat api membesar saat ditaburkan ke atas api? Gara-gara video viral tersebut, kopi kemasan sempat diduga menggunakan bubuk mesiu dalam komposisi kopinya. Wah kira-kira benar nggak ya? &#8230;",
          "date_published": "2019-09-23T10:41:52+07:00",
          "date_modified": "2019-10-07T17:39:50+07:00",
          "author": {
            "name": "Feby",
            "url": "https://edutore.com/news/author/feby-dyastika/",
            "avatar": "https://secure.gravatar.com/avatar/a5d22df5938094e29139aa4de938dfc9?s=512&d=mm&r=g"
          },
          "image": "https://news.edutore.com/wp-content/uploads/2019/03/Kopi-Instant_title.jpg",
          "tags": [
            "kopi",
            "mesiu",
            "terbakar",
            "viral",
            "Sains dan Teknologi"
          ],
          "attachments": [
            [
              {
                "url": "https://news.edutore.com/wp-content/uploads/2019/09/Dust-Explosions-Cool-Science-Experiment.mp4",
                "mime_type": "video/mp4",
                "size_in_bytes": 9336567
              }
            ]
          ]
        },
        {
          "id": "https://edutore.com/news/cara-memaksimalkan-diri-di-perkuliahan/",
          "url": "https://edutore.com/news/cara-memaksimalkan-diri-di-perkuliahan/",
          "title": "Cara Memaksimalkan Diri di Perkuliahan",
          "content_html": "Mahasiswa baru di beberapa universitas sudah mulai aktif perkuliahan. Bagaimana dengan kamu Edufriends? Nggak sedikit mahasiswa baru merasa bingung bagaimana beradaptasi dengan dunia perkuliahan yang jauh berbeda dengan sekolah. Menjadi mahasiswa artinya kita dituntut menjadi dewasa dengan menentukan masa depan &#8230; ",
          "content_text": "Mahasiswa baru di beberapa universitas sudah mulai aktif perkuliahan. Bagaimana dengan kamu Edufriends? Nggak sedikit mahasiswa baru merasa bingung bagaimana beradaptasi dengan dunia perkuliahan yang jauh berbeda dengan sekolah. Menjadi mahasiswa artinya kita dituntut menjadi dewasa dengan menentukan masa depan &#8230;",
          "date_published": "2019-09-09T09:09:25+07:00",
          "date_modified": "2019-09-09T10:22:19+07:00",
          "author": {
            "name": "Feby",
            "url": "https://edutore.com/news/author/feby-dyastika/",
            "avatar": "https://secure.gravatar.com/avatar/a5d22df5938094e29139aa4de938dfc9?s=512&d=mm&r=g"
          },
          "image": "https://news.edutore.com/wp-content/uploads/2019/08/cara-memaksimalkan-diri-di-perkuliahan_landscape.jpg",
          "tags": [
            "campus",
            "kuliah",
            "organisasi",
            "Tips dan Trik",
            "tips kuliah"
          ]
        }
      ]
    }
}
