import Vue from 'vue'
import moment from 'moment'
import eduLoader from '~/components/loader/base_loader.vue'
import { mapState } from 'vuex'

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();
Vue.mixin({
  methods: {
    shuffleArray(a){
      for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
      }
      return a;
    },
    convertIsoToDate(_date){
      // let date = new Date('2013-08-03T02:00:00Z');
      // let year = date.getFullYear();
      // let month = date.getMonth()+1;
      // let dt = date.getDate();
      // var str = '2011-04-11T10:20:30Z';
      let date = moment(_date);
      // let dateComponent = date.utc().format('DD MMMM YYYY');
      // let timeComponent = date.utc().format('HH:mm:ss');
      // console.log(dateComponent);
      // console.log(timeComponent);
      return date.utc().format('DD MMM YYYY');
    },
    aCommonMethod () {
        return 'hello from mixin'
    },
    globsFunc(){
        return 'globs from mixins'
    },
    loadingMode(text, loadingStatus) {
      let loader = this.$loading.show({
          // Optional parameters
          // container: this.fullPage ? null : this.$refs.baselayout,
          container: this.$refs.baselayout,
          // container: null,
          canCancel: false,
          onCancel: this.onCancel,
          transition: 'fade',
          // isFullPage: false,
          // opacity: 0.8
        },{
          // Pass slots by their names
          default: this.$createElement(eduLoader),
        }
      );
      this.$store.commit('loading/setTextLoading', {text:text, loader: loader})
      // simulate AJAX
      // setTimeout(() => {
      //   this.rLoader.hide()
      // }, 50000)
    },
    GzeroFill(val){
        let n = val
        let l = 7
        return ('0000000000'+n).slice(-l);
      },
    currencyFormat(n, currency){
        // WITH COMMA DIVIDER
        return currency + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "," + c : c;
        });
    },
    currencyFormat2(n, currency){
      // WITH DOTS DIVIDER
      return currency + n.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
      });
    },
    countNilai(jmlsoal, jwbnBenar){
        
        return ('0000000000'+n).slice(-l);
      },
    moduleCategory(data){
        switch (data) {
            case 1:
            return 'bg-gold'
            case 2: 
            return 'bg-blue'
            case 4:
            return 'bg-brown'
          }
    },
    moduleCategory2(data){
      switch (data) {
        case 1:
          return 'ico label-crown-1'
        case 2:
          return 'ico label-crown-3'
        case 4:
          return 'ico label-crown-4'
      }
    },
    htmlEntitiesConverter(htmlEntities){
        // console.log(entities.decode('&lt;&gt;&quot;&apos;&amp;&copy;&reg;&#8710;')); // <>"'&&copy;&reg;∆
        return entities.decode(htmlEntities)
    },
    reLogin(){
        this.$store.dispatch('auth/reLogin')
    },
    userSessionExpired(){
      this.$nuxt.$router.push('/session-expired')
    },
    groupBy(list, keyGetter) {
      const map = new Map();
      list.forEach((item) => {
        const key = keyGetter(item);
        const collection = map.get(key);
        if (!collection) {
          map.set(key, [item]);
        } else {
          collection.push(item);
        }
      });
      return map;
    },
    getTgl(){
      let tgl = []
      for (let x = 1; x <= 31; x++){
        tgl.push(x)
      }
      // console.log(tgl)
      return tgl
    },
    getBulan(){
      return ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
    },
    getTahun(){
      let dt = new Date()
      // console.log(dt.getFullYear())
      let thn = []
      for (let x = dt.getFullYear(); x >= dt.getFullYear()-100; x--){
        thn.push(x)
      }
      // console.log(thn)
      return thn
    },
    getAuser(){
      console.log(this.$store.getters['auth/user'])
    },
    async loadMySubs() {
      // 1X
      // console.log('hello')
      this.$store.dispatch('langganan/setMySubscriptions')
        .then((response) => {
          // this.sLoading = false
          if (response.success){
            // console.log(response.data.rows)
            response.data.rows.forEach(element => {
              // console.log(element.id)
              this.loadMyModuleSubs(element.id)
              })
            // this.mySubscriptions = response.data.rows
            // this.checkMyModule()
          }else{
            // console.log(response)
            if (response.data.message === 'Token is Invalid'){
              this.reLogin()
            }
          }

        })
    },
    async loadMyModuleSubs(id_langganan) {
      // console.log('hello')
      // TERGANTUNG LOOP
      this.$store.dispatch('langganan/getMyModuleSubscriptions', id_langganan)
        .then((response) => {
          // this.sLoading = false
          // console.log(response)
          if (response.success){
            // this.mySubscriptions = response.data.rows
            // this.checkMyModule()
          }else{
            // console.log(response)
            if (response.data.message === 'Token is Invalid'){
              this.reLogin()
            }
          }

        })
    },
  },
})
