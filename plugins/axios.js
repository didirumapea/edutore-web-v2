export default function ({store, $axios, redirect }) {
  // Set baseURL (both client and server)
  // $axios.setBaseURL('http://api.example.com')

// Change URL only for client
//   if (process.client) {
//     // console.log('on client')
//     $axios.defaults.setBaseURL = 'api/'
//   }

// Change URL only for server
  if (process.server) {
    // console.log('on server')
    // $axios.setBaseURL(process.env.API_URL)
    $axios.defaults.baseURL = 'https://api.edutore.com/'
  }

  // Create a custom axios instance
  $axios.defaults.headers.common = {
    'On-Project': 'edutore-x-2020',
    'Signature-Key': 'tpsRXbUIl6/iHZ4ZS9gqjz+JnY3qSsr0WESiu6QPRoz0LvU+W+x2drxzi796KcH6',
  };
    $axios.onRequest(config => {
      if (process.env.NODE_ENV === 'development'){
        console.log('SPY: ' + config.url)
        // console.log( process.env.BASE_URL)
      }
    })
  }
