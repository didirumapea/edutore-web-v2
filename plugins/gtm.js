/* eslint-disable */

export default ({ app }) => {

    // ADS SIDE BAR
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd.push(function() {
        googletag.defineSlot('/21860443230/partner_post_sidebar', [300, 600], 'div-gpt-ad-1582795710220-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });

    // ADS BOTTOM
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd.push(function() {
        googletag.defineSlot('/21860443230/partner_main_bottombar', [728, 90], 'div-gpt-ad-1582795775605-0').addService(googletag.pubads());
        googletag.pubads().enableSingleRequest();
        googletag.enableServices();
    });

    // SAMPEL
    if (process.client){
        window.googletag = window.googletag || {cmd: []};
        googletag.cmd.push(function() {
            googletag.defineSlot('/21860443230/Frontpage_Hero_1366x468', [1366, 468], 'div-gpt-ad-1585726432401-0').addService(googletag.pubads());
            googletag.pubads().enableSingleRequest();
            googletag.enableServices();
        });
    }


}