import createPersistedState from 'vuex-persistedstate'

export default ({ store, isHMR }) => {
	if (isHMR) return

	window.onNuxtReady(() => {
		createPersistedState({
			key: 'vuex', // key
			// paths: ['auth.count']
			paths: ['auth', 'edutest', 'helper.name', 'settings'] // store file name
		})(store)
	})
}
