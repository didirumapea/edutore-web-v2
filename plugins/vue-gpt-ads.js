import AdManager from 'vue-google-ad-manager';
 
let mappings = {
    banner:[
        { window :[0,0], sizes: [ [320,50] ] },
        { window :[980,0], sizes: [ [720,60],[728,90] ] }
    ],
    rectangle:[
        { window: [0, 0], sizes: [ [300, 250] ] },
        { window: [980, 0], sizes: [ [300, 250] ] }
    ]
}
 
let sizes = {
    banner: [ [720, 60],[728, 90],[320, 50] ],
    rectangle: [ [300, 250] ]
};
 
Vue.use(AdManager, {
    id: '21860443230',
    mappings,
    sizes //optional
});